package games;

import java.util.ArrayList;

import util.Constant;
import util.Util;

public class BlackjackGames{
//	final int BET_DEFAULT = 10;
	ArrayList<Blackjack> games = new ArrayList<Blackjack>();
	public String hit(String name){
		int coord = coord(name);
		if(coord == -1){ return noGame(name); }
		else{
			Blackjack game = games.get(coord);
			game.addCard(false);
			String reply = game.status();
			if(game.value(false) > 21){ // Bust - dealer win, so remove that game and no gain for player.
				reply += "\n" + Util.noCall(name) + " busted - Dealer wins. | 게임이 종료되었습니다.";
//				int bet = games.get(coord).getBet();
//				if(bet != 0){ reply += " 돈 변화 : -" + bet; }
				games.remove(coord);
			}
			return reply;
		}
	}
	public String stand(String name){
		int coord = coord(name);
		if(coord == -1){ return noGame(name); }
		else{
			Blackjack game = games.get(coord);
			String reply = "";
			reply += game.stand();
			int bet = game.getBet();
			if(game.value(true) > 21){ // dealer bust
				reply += "\n" + "Dealer busted - " + Util.noCall(name) + " wins. | 게임이 종료되었습니다.";
//				if(bet != 0){ reply += " 돈 변화 : +" + bet; }
//				Constant.money.add(name, 2*bet);
				games.remove(coord);
			}
			else if(game.value(true) > game.value(false)){ // dealer win
				reply += "\n" + "Dealer wins. | 게임이 종료되었습니다.";
//				if(bet != 0){ reply += " 돈 변화 : -" + bet; } 
				games.remove(coord);
			}
			else if(game.value(true) < game.value(false)){ // player win
				reply += "\n" + Util.noCall(name) + " wins. | 게임이 종료되었습니다.";
//				if(bet != 0){ reply += " 돈 변화 : +" + bet; }
//				Constant.money.add(name, 2*bet);
				games.remove(coord);
			}
			else{ // draw case.
				reply += "\n" + "Draw. | 게임이 종료되었습니다.";
				if(bet != 0){ reply += " 돈 변화 : 0"; }
				Constant.money.add(name, bet);
				games.remove(coord);
			}
			return reply;
		}
	}
	public String doubleD(String name){ // doubleDown
		int coord = coord(name);
		if(coord == -1){ return noGame(name); }
		else{
			Blackjack game = games.get(coord);
			if(game.player.size() != 2){
				return "더블다운은 게임을 시작한 직후(패가 2장)일때만 실행할 수 있습니다!";
			}
			int bet = games.get(coord).getBet();
			int balance = Constant.money.getBalance(name);
//			if(balance < bet){
//				return "돈이 충분하지 않습니다. | 현재 소지금 : " + balance + " | 현재 베팅액 : " + bet;
//			}
			Constant.money.remove(name, bet);
			game.bet *= 2;
			// Auto-hit
			String reply = hit(name);
			if(game.value(false) > 21){ // Bust - dealer win, so remove that game and no gain for player.
				return reply;
			}
			// Stand after hit
			reply += "\n" + stand(name);
			return reply;
		}
	}
	String start(String name, String input){
//		int bet = BET_DEFAULT; // Default Betting
//		if(input.length() != 0){
//			bet = Integer.parseInt(input);
//			if(bet < 0){ return "시방 감히 베팅가지고 사기질이여?"; }
//		}
		int money = Constant.money.getBalance(name);
//		if(money < bet){ return "돈이 충분하지 않습니다. | " + Util.noCall(name) + "의 현재 소지금 : " + money; }
//		Constant.money.remove(name, bet);
		Blackjack game = new Blackjack(name, 0); // bet -> 0
		games.add(game);
		String reply = "";
//		if(bet == 0){ reply += "돈을 걸지 않고"; }
//		else{ reply += "" + bet + "만큼을 걸고"; }
		reply += " 블랙잭 게임을 시작합니다.\n"; 
		reply += game.status(); 
		if(game.value(false) == 21){
			reply += "\nBLACKJACK!\n";
			game.open = true;
			reply += game.status() + "\n";
			if(game.value(true) == 21){
				reply += "Dealer is also blackjack : Draw. | 게임이 종료되었습니다.";
//				if(bet != 0){ reply += " 돈 변화 : 0"; }
//				Constant.money.add(name, bet);
			}
			else{
				reply += "" + Util.noCall(name) + " won by BLACKJACK. | 게임이 종료되었습니다.";
//				if(bet != 0){ reply += " 돈 변화 : +" + 2*bet; }
//				Constant.money.add(name,3*bet);
			}
			games.remove(game);
		}
		return reply;
	}
	public String process(String name, String input){
		boolean isOnlyNumber = (input.length() != 0);
		// *블랙잭 30 -> 30을 걸고 시작하기 위해서. "*블랙잭"의 경우는 정보를 출력해줘야 해서 예외처리 겸.
		for(int i=0;i<input.length();i++){
			int charValue = (int)input.charAt(i);
			if(charValue == 32 || (charValue >= 48 && charValue <= 57)){}
			else{ isOnlyNumber = false; break; }
		}
		if(isOnlyNumber){
			return start(name, input.substring(1)); // 맨 앞의 스페이스 처리를 위한것.
		}
		else if(input.startsWith(" 시작") || input.toLowerCase().startsWith(" start")){ // *블랙잭 시작
			input = input.replace(" 시작", ""); input = input.toLowerCase().replace(" start", ""); // 시작, start란 단어 제거
			if(input.length() != 0 && input.charAt(0) == ' '){ input = input.substring(1); }
			if(coord(name) != -1){
				return Util.noCall(name) +
						"은 현재 블랙잭 게임을 진행중입니다! 현재 게임 정보는 *블랙잭 정보를 이용하세요.";
			}
			return start(name, input);
		}
//		else if(input.startsWith(" 베팅")){ // *블랙잭 베팅
//			return "현재 기본 베팅액 : " + BET_DEFAULT;
//		}
		else if(input.startsWith(" 정보")){ // *블랙잭 정보
			int coord = coord(name);
			if(coord(name) == -1){
				return Util.noCall(name) + "은 현재 블랙잭 게임을 진행중이지 않습니다!";
			}
			return games.get(coord).status();
		}
		else if(input.startsWith(" 명령")){ // *블랙잭 명령
			return "*힛, *hit : 한 장을 더 받습니다. | " +
					"*스탠드(*스), *stand : 현재 상태에서 멈추고 딜러의 패를 확인합니다.\n" +
					"*블랙잭 시작 <베팅>, *blackjack start <베팅> : <베팅>만큼을 베팅하는 블랙잭 게임을 시작합니다. " +
//					"베팅 부분을 비울 경우 기본값인 " + BET_DEFAULT + "만큼 베팅을 하고 게임을 시작합니다. "+
					"\"시작\" 또는 \"start\"를 비워도 시작이 가능합니다.\n" +
					"*더블, *double : 더블다운(베팅액을 두배로 늘리고 한장만을 받고 Stand합니다. | " +
					"*블랙잭 규칙, *blackjack rule : 블랙잭 게임 규칙을 봅니다.\n" +
					"*블랙잭 명령 : 블랙잭 게임 명령어들을 봅니다. | " +
					"*블랙잭 설명 : 블랙잭 게임 설명을 봅니다. | " +
					"*블랙잭 정보 : 현재 플레이어가 진행중인 게임 정보를 봅니다. | " +
					"*블랙잭 베팅 : 블랙잭 게임의 기본 베팅액을 봅니다.";
		}
		else if(input.startsWith(" 규칙")){ // *블랙잭 규칙
			return "A=1 or 11, J~K=10으로, 21 이하의 수 중 가장 21에 가까운 숫자를 만드는 것이 목표인 게임입니다.\n" +
					"단 두 장만으로 21을 완성하면 블랙잭으로, 이 조건으로 승리시 보상이 1.5배가 됩니다.";
		}
		else if(input.startsWith(" 설명")){ // *블랙잭 설명
			return "일반적인 규칙을 따르는 블랙잭 게임입니다. 여러 가지 룰 중 더블다운만 적용되어 있습니다.\n" +
					"또한 딜러는 16이하에선 Hit, 17이상에선 Stand라는 규칙을 적용합니다.";
		}
		else{
			return "블랙잭 게임입니다. 자세한 설명은 *블랙잭 설명을, 명령어들은 *블랙잭 명령을 이용해주세요.";
		}
	}
	String noGame(String name){ // message - when there is no game processing for that nick.
		return "현재 " + Util.noCall(name) + "은 블랙잭 게임을 하는 중이 아닙니다!";
	}
	int coord(String name){
		for(int i=0;i<games.size();i++){
			if(Util.isSame(games.get(i).name, name)){ return i; }
		}
		return -1; // there is no name's playing game.
	}
}