package games;

import util.*;

import java.util.ArrayList;
import java.util.Random;

class Card{
	final String[] SHAPE = {"♠", "♥", "♣", "◆"};
	int shape; // 0~3
	int number; // 1(A)~13(K)
	
	Card(){
		shape = new Random().nextInt(4);
		number = 1 + new Random().nextInt(13);
	}
	boolean isEqual(Card input){
		return this.shape == input.shape && this.number == input.number;
	}
	int point(){
		if(number > 10){ return 10; }
		return number;
	}
	String name(){
		String reply = SHAPE[shape];
		switch(number){
		case 1: reply += "A"; break;
		case 11: reply += "J"; break;
		case 12: reply += "Q"; break;
		case 13: reply += "K"; break;
		default: reply += String.valueOf(number);
		}
		return reply;
	}
}

class Blackjack{
	final String UNKNOWN = "??";
	String name;
	int bet;
	ArrayList<Card> dealer = new ArrayList<Card>();
	ArrayList<Card> player = new ArrayList<Card>();
	boolean open = false; // whether dealer's deck is all opened
	Blackjack(String name, int bet){
		this.name = name;
		this.bet = bet;
		int count = 0;
		while(count < 4){
			Card newCard = new Card();
			if(!isDuplicated(newCard)){
				if(count < 2){ dealer.add(newCard); }
				else{ player.add(newCard); }
				count++;
			}
		}
	}
	String status(){
		String dealerDeck = "Dealer : ";
		if(open){ dealerDeck += String.valueOf(value(true)) + " (" + dealer.get(0).name(); }
		else{ dealerDeck += "(" + UNKNOWN; }
		for(int i=1;i<dealer.size();i++){
			dealerDeck += " " + dealer.get(i).name();
		}
		dealerDeck += ")";
		
		String playerDeck = "" + Util.noCall(name) + " : ";
		playerDeck += String.valueOf(value(false));
		playerDeck += " (";
		for(int i=0;i<player.size();i++){
			if(i != 0){ playerDeck += " "; }
			playerDeck += player.get(i).name();
		}
		playerDeck += ")";
		
		return dealerDeck + " | " + playerDeck;
	}
	int getBet(){
		return bet;
	}
	int value(boolean isDealer){
		boolean ace = false; // if there is ace, then this value become true.
		ArrayList<Card> pack;
		if(isDealer){ pack = dealer; }
		else{ pack = player; }
		int value = 0;
		for(int i=0;i<pack.size();i++){
			value += pack.get(i).point();
			ace |= pack.get(i).point() == 1; // ace <- ace or [pack.get(i) is ace]
		}
		if(value < 12 && ace){ value += 10; }
		return value;
	}
	boolean isDuplicated(Card input){ // Return whether that card on dealer or player.
		for(int i=0;i<dealer.size();i++){
			if(dealer.get(i).isEqual(input)){
				return true;
			}
		}
		for(int i=0;i<player.size();i++){
			if(player.get(i).isEqual(input)){
				return true;
			}
		}
		return false;
	}
	void addCard(boolean toDealer){
		while(true){
			Card newCard = new Card();
			if(!isDuplicated(newCard)){
				if(toDealer){ dealer.add(newCard); }
				else{ player.add(newCard); }
				break;
			}
		}
	}
	String stand(){ // Dealer's turn(after player said stand)
		String reply = "";
		open = true;
		while(value(true) < 17){
			reply += status() + "\n";
			addCard(true);
		}
		reply += status();
		return reply; // 수정 가능성 높음.
	}
}