package games;

import util.*;

public class Chaser extends ChaserElem{
	String nick;
	String[] rank = {"CS", "St", "ES", "FD", "FH", "Ch", "6s", "5s", "4s", "3s", "2s", "1s"};
	String[] diceResult = new String[12];
	int[] score = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	public Chaser(String nick, boolean sort){ this.nick = nick; autosort = sort;}
	public String dicePrint(){ // MyBot에서 호출옴
		if(isFilled()){ return "게임이 종료되어 있습니다!"; }
		if(dice[0] == 0){ return "주사위를 굴리기 전입니다!"; }
		return Util.noCall(nick) + "의 주사위 눈 : " + dice[0] + "" + dice[1] + "" + dice[2] + "" + dice[3] + "" + dice[4]
				+ "(현재 " + count + "번 굴림)";
	}
	public String dicePrint(boolean[] bold){ // 이 안에서 호출옴
		String ans = "";
		for(int i=0;i<5;i++){
			if(bold[i]){
				ans += "" + dice[i] + "";
			}
			else{
				ans += dice[i];
			}
		}
		return ans;
	}
	public String put(String part){
		String ans = "";
		for(int i=0;i<12;i++){
			if(Util.isSame(part.toLowerCase(), rank[i].toLowerCase())){
				if(score[i] == -1){
						score[i] = getScore(i);
						diceResult[i] = dice[0] + "" + dice[1] + "" + dice[2] + "" + dice[3] + "" + dice[4];
						ans += rank[i] + "에 " + score[i] + "점(" + diceResult[i] + ")을 추가하였습니다.";
						diceInit();
						break;
				}
				else{
					ans = rank[i] + " : 이미 저장되어 있습니다.(" + diceResult[i] + "/" + score[i] +"점)"; 
				}
			}
		}
		if(ans.length() == 0){ ans = part + "에 해당하는 족보 칸은 없습니다!"; }
		if(isFilled()){ ans += "\n게임이 종료되었습니다. 총합 : " + score() + "\n" + printRank();	}
		return ans;
	}
	public boolean auto(){ return autosort; }
	public String getNick(){ return nick; }
	public void autoConvert(){ autosort = !autosort; }
	public String roll(String save){
		System.out.println(save);
		if(isFilled()){ return "게임이 종료되어 있습니다."; }
		if(count == 3){ return "가능한 주사위 굴리기 시도를 모두 사용하셨습니다!"; }
		else{
			set(getSave(save));
			return Util.noCall(nick) + "의 " + count + "번째 굴리기 결과 : " + dicePrint(getSave(save));
			// getSave : to make fixed thing bold	
		}
	}
	public String printRank(){
		String ans = Util.noCall(nick) + " = " + score();
		for(int i=0;i<12;i++){
			ans += " | ";
			if(score[i] == -1){ ans += rank[i] + "--"; }
			else{ ans += rank[i] + to2digit(score[i]) + "(" + diceResult[i] + ")"; }
		}
		return ans;
	}
	String to2digit(int a){
		if(a<10){ return "0" + a; }
		else{ return Integer.toString(a); }
	}
	public int score(){
		int sum = 0;
		for(int i=0;i<12;i++){
			if(score[i] != -1){ sum += score[i]; } 
		}
		return sum;
	}
	boolean isFilled(){
		for(int i=0;i<12;i++){
			if(score[i] == -1){ return false; }
		}
		return true;
	}
	void diceInit(){
		for(int i=0;i<5;i++){ dice[i] = 0; }
		count = 0;
	}
	int getScore(int input){ // input == coord
		sort();
		switch(input){
		case 0: // CS = aaaaa
			if(dice[0] == dice[4]){ return 50; }
			else{ return 0; }
		case 1: // St = 12345
			if(dice[0] == 1 && dice[1] == 2 && dice[2] == 3 && dice[3] == 4 && dice[4] == 5){ return 40; }
			else{ return 0; }
		case 2: // ES = 23456
			if(dice[0] == 2 && dice[1] == 3 && dice[2] == 4 && dice[3] == 5 && dice[4] == 6){ return 30; }
			else{ return 0; }
		case 3: // FD = aaaab or abbbb
			if(dice[0] == dice[3] || dice[1] == dice[4]){ return dice[0]+dice[1]+dice[2]+dice[3]+dice[4]; }
			else{ return 0; }
		case 4: // FH = aaabb or aabbb
			if(dice[0] == dice[1] && dice[3] == dice[4]){
				if(dice[1] == dice[2] || dice[2] == dice[3]){ return dice[0]+dice[1]+dice[2]+dice[3]+dice[4]; }
			}
			return 0;
		case 5: // Ch = aabbc or aabcc or abbcc
			if(dice[0] == dice[1] && dice[2] == dice[3] ||
				dice[0] == dice[1] && dice[3] == dice[4] ||
				dice[1] == dice[2] && dice[3] == dice[4]){ return dice[0]+dice[1]+dice[2]+dice[3]+dice[4]; }
			else{ return 0; }
		default: // 6s(6)~1s(11)
			int sc = 0;
			for(int i=0;i<5;i++){
				if(dice[i] == 12-input){
					sc += 12-input;
				}
			}
			return sc;
		}
	}
	public boolean[] getSave(String input){
		boolean[] renew = {true, true, true, true, true};
		if(input.contains("t")){
			if(input.length() == 5){
				for(int i=0;i<5;i++){
					System.out.println(input.charAt(i) + " ");
					if(input.charAt(i) == (char)(dice[i]+48)){ renew[i] = false; }
					else if(input.charAt(i) != 't'){ return null; }
				}
			}
			else{ return null; }
			return renew;
		}
		else if(input.length()>5){ return null; } // error 
		else{
			while(true){
				if(input.length()==0){ return renew; }
				boolean check = false; // whether checking it is concerned
				int save = Integer.parseInt(input.substring(0,1));
				for(int i=0;i<5;i++){
					if(renew[i] && dice[i] == save){
						renew[i] = false;
						check = true;
						break;
					}
				}
				if(!check){
					return null;
				}
				input = input.substring(1);
			}
		}
	}
}
class ChaserElem{
	protected int[] dice = {0,0,0,0,0};
	protected int count = 0; // 0(pre) -> 3(fin)
	protected boolean autosort;
	int[] getDice(){ return dice; }
	void set(boolean[] input){
		for(int i=0;i<5;i++){
			if(input[i]){
				dice[i] = Util.dice();
			}
		}
		if(autosort){ sort(); }
		count++;
	}
	void sort(){
		for(int i=1;i<5;i++){
			for(int j=0;j<i;j++){
				if(dice[j]>dice[i]){ swap(i,j); }
			}
		}
	}
	void swap(int a, int b){
		int tmp=dice[a]; dice[a]=dice[b]; dice[b]=tmp;
	}
}
