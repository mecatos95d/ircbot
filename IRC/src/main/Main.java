package main;
public class Main{
	public static void main(String[] args) throws Exception {
		System.setProperty("file.encoding", "UTF-8");

		// On developing, it is true. On normal usage, it is false.
		// This modifies channel joining, and nick of this bot.
		final boolean TEST_MODE = false;
		String patchDate = "2017/07/19";
		String patchNote =
				"1) 체이서 명령 출력 교정" +
						"\n2) 명령 순서 재조정" +
						"\n3) 치명적 버그 수정" +
						"\n4) 깃헙주소 출력 명령 추가" +
						"\n5) 순열 명령 일반텍스트 대응 확장";

		// Now start our bot up. BS = Bot Structure
		MyBot BS = new MyBot(TEST_MODE, patchNote, patchDate);

		String[] channel = { "uncyclopedia", "MeltCraft", "TOZ", "ruree", "Revi", "전자소리놀이", "XGC" };
		String[] channelDev = { "TOZ" };

		String[] channelJoin;
		if(TEST_MODE){ channelJoin = channelDev; }
		else{ channelJoin = channel; }

		for(int i=0;i<channelJoin.length;i++){
			BS.joinChannel("#" + channelJoin[i]);	// Join to channels.
			//			BS.sendMessage(channelJoin[i], "안녕하세요, " + BS.getNick() + "입니다.");
		}
	}
}