package main;
public class OrderList {
	static String orders =
			"명령어 목록 : *정보 *명령 *나무(*namu) *미러 *핑(*ping) *퐁(*pong) *cc(카이사르 암호) *cca(=cc 0~25) " +
			"*접속(*join) *나가(*leave) *프야매(*PBM) *주인 트위터 *오일러(*PE) *초퀴즈" +
			"*푸시(*puush) *대칭(*거울, *refl) *블랙잭(blackjack) *피타(py)" +
//			"*크리퍼 *세종 *세종0 *함정(*트랩, *trap)" +
			"*시간(*time) *래더(*elo) *순열(*rp) *SNQ(역번호퀴즈)\n" +
			"*j, 유(유비트), *p, 팝(팝픈) *s, 사(사볼), *r, 리(리플렉) + <ID>: 인포 연결 명령 \n" +
//			"| 살아있네(문장 중 포함되면 자동실행)" +
			"*주사위(*dice) : *주사위 k,n : 1~k가 나오는 주사위를  n번 굴립니다 | n=1 축약형 : *주사위 k \n" +
			"시간 연산 : hh:mm - hh:mm, hh:mm:ss - hh:mm:ss꼴 자동인식 - AM/PM hh꼴도 인식\n" +
//			"*배틀 : *배틀 정보 (캐릭터) / *배틀 추가 (캐릭터) / *배틀 등록 (캐릭터) / *배틀 시작 (선공) (후공) \n" +
//			"*미네랄(*mineral), *가스(*gas), *멀티(*multi), *자원상태(*resource), *소유(*amount) : " + 
//			"채널별 미네랄/가스(스타1기준)" +
			"";
	static String getOrders(){
		return orders;
	}
}
