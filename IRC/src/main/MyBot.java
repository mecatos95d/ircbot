package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

import money.MoneyDB;

import org.jibble.pircbot.*;

import games.*;
import quiz.*;
import util.*;
import util.simple.Elo;

public class MyBot extends PircBot{
	String patch; // 패치노트
	String patchDate; // 패치일자(버전 대신 사용)
	Date INIT_TIME = Calendar.getInstance().getTime();
	RGinfo RGI = new RGinfo();

	MyBot(boolean DEV_MODE, String patch, String patchDate) throws NickAlreadyInUseException, IOException, IrcException{
		this.patch = patch;
		this.patchDate = patchDate;
		String nick;
		if(DEV_MODE){ nick = Constant.DEV_NICK; }
		else{ nick = Constant.NORMAL_NICK; }
		final String login = Constant.ID;
		this.setName(nick);
		this.changeNick(nick);
		this.setLogin(login);
		//System.out.println(BS.getNick());

		// Enable debugging output.
		setVerbose(false);

		// Connect to the IRC server.
		connect("webirc.ozinger.org");

		// Setting bot's name.
		changeNick(getNick());

		OrderManager.init();
		banInit();
		RGI.init();
	}
	// Invite 처리
	protected void onInvite(String targetNick,
			String sourceNick, String sourceLogin, String sourceHostname,String channel){
		System.out.println(targetNick + " " + sourceNick + " " + sourceLogin + " " + sourceHostname);
		if(Util.isSame(targetNick, getName())){ // 의미 없지만 혹시 몰라서..
			if(Util.isBanned(sourceNick, sourceLogin)){ return; } // 밴당한 유저가 초대시
			String[] chanList = getChannels();
			for(int i=0;i<chanList.length;i++){
				if(Util.isSame(chanList[i], channel)){ return; } // 이미 채널에 들어가있을경우
			}
			joinChannel(channel);
		}
	}
	public void banInit() throws FileNotFoundException{
		String filename = "./data/Blacklist.txt";
		Scanner inputStream = new Scanner(new File(filename));
		while(inputStream.hasNextLine()){ Constant.BAN.add(inputStream.nextLine()); }
		inputStream.close();
		System.out.println("BAN LIST 초기화 완료");
	}
	/** -------------------------------------------------- */
	// Break and Send message : asdf\nqwer -> send asdf and qwer
	// Query
	public void onPrivateMessage(String sender, String login, String hostname, String message){
		message = message.trim();
		if(Util.isAdmin(login)){ // op-mode for this bot(My nick)
			if(message.startsWith("*msgCh ")){
				String tmp = message.substring(7);
				int div = tmp.indexOf(" ");
				String channel = tmp.substring(0, div);
				String reply = tmp.substring(div+1);
				//System.out.println("#"+channel + "|" + reply); // testing
				sendMessage("#"+channel, reply);
			}
			if(message.startsWith("*quit ")){
				String reason = message.substring(6);
				quitServer(reason);
			}
		}
		onMessage(sender, sender, login, hostname, message);
	}
	// message reactor part
	// Each : channel : channel, sender = nick, login = ???, hostname = IP, message = message
	public void onMessage(String channel, String sender, String login, String hostname, String message){
		if(message.contains(Constant.NORMAL_NICK)){
			sendMessage(channel,  "...(*'ω'*)...");
		}
		ArrayList<String> content = Util.parse(message.trim());
		
		message = message.trim();
		if(login.toLowerCase().contains("bot")){ return; }
		System.out.println(channel + " / " + sender + " / " + login + " / " + hostname + " / " + message);
		
		// for Bot checking. MeltBot is exception.
		/** 밴 설정 -------------------------------------------------- */
		if(Util.isBanned(sender, login)){ return; }
		/** Constant.ADMIN operations -------------------------------------------------- */
		if(Util.isAdmin(login)){
			if(message.startsWith("*관리")){ // 관리자 전용 명령어 출력
				String operators = "*종료(*quit, *exit) <사유>, *밴닉, *밴등록, *b, *d, *채널, *공지 <공지>, "
						+ "*소지금 <닉>, *돈-, *정리";
				sendMessage(Constant.ADMIN_NICK, Constant.BOLD + "관리자 명령들" + Constant.BOLD);
				sendBreakMessage(Constant.ADMIN_NICK, operators);
			}
			/**
			 * 추가 필요 : 밴 변경
			 */
			if(message.startsWith("*채널")){
				String[] chanList = getChannels();
				String reply = "현재 접속한 채널 리스트 :";
				for(int i=0;i<chanList.length;i++){
					if(i % 10 == 0){ reply += "\n"; }
					reply += chanList[i] + " ";
				}
				sendBreakMessage(Constant.ADMIN_NICK, reply);
			}
			if(message.startsWith("*공지 ")){
				String announce = Constant.BOLD + "<공지> " + Constant.BOLD + message.substring(4);
				String[] chanList = getChannels();
				for(int i=0;i<chanList.length;i++){
					sendBreakMessage(chanList[i], announce);
				}
			}
			if(message.startsWith("*정리")){
				// 채널에 혼자 있는경우 나가기(생성채널에 끌려갔다가 다른 사람들이 모두 나갔을경우
				String[] chanList = getChannels();
				String leftChans = "";
				int leftSize = 0;
				for(int i=0;i<chanList.length;i++){
					if(getUsers(chanList[i]).length == 1){
						partChannel(chanList[i], "사람이 없네...");
						leftChans += chanList[i] + " "; leftSize++;
						if(leftSize % 10 == 0){ leftChans += "\n"; }
					}
				}
				if(leftSize != 0){
					sendMessage(Constant.ADMIN_NICK, "채널 정리를 통해서 " + leftSize + "개 채널을 정리했습니다.");
					sendBreakMessage(Constant.ADMIN_NICK, leftChans); // 정리된 채널 리스트
				}
			}
			if(message.startsWith("*종료") || message.toLowerCase().startsWith("*exit")
					|| message.toLowerCase().startsWith("*quit")){
				int coord = message.indexOf(' ')+1;
				String reason;
				if(coord != 0){ reason = message.substring(coord); }
				else{ reason = "SHUTDOWN"; }
				quitServer(reason); System.exit(0);
			}
		}
		/** Ping parts */
		if(message.startsWith("*핑")){ sendMessage(channel, sender + ", 퐁."); }
		if(message.startsWith("*퐁")){ sendMessage(channel, sender + ", 핑. ...어라?"); }
		if(message.startsWith("*ping")){ sendMessage(channel, sender + ", pong."); }
		if(message.startsWith("*pong")){ sendMessage(channel, sender + ", ping. ...?"); }
		
		/** Chaser Games & SNQ Reply --- Moved from below */
		chaser(sender, channel, content);
		sendBreakMessage(channel, Constant.snq.process(sender, channel, message));
		
		/** Blackjack Games -------------------------------------------------- */
		if(message.startsWith("*힛") || message.toLowerCase().startsWith("*hit")){
			sendBreakMessage(channel, Constant.blackjack.hit(sender));
		}
		if(message.startsWith("*스탠드") || Util.isSame(message, "*스") || message.toLowerCase().startsWith("*hit")){
			sendBreakMessage(channel, Constant.blackjack.stand(sender));
		}
		if(message.startsWith("*더블") || message.toLowerCase().startsWith("*double")){
			sendBreakMessage(channel, Constant.blackjack.doubleD(sender));
		}
		if(message.startsWith("*블랙잭")){ // "*블랙잭" 으로 시작하는 명령들
			sendBreakMessage(channel, Constant.blackjack.process(sender, message.substring(4)));
		}
		if(message.toLowerCase().startsWith("*blackjack")){ // "*blackjack" 으로 시작하는 명령들
			sendBreakMessage(channel, Constant.blackjack.process(sender, message.substring(10)));
		}
		/** -------------------------------------------------- */
		if(message.startsWith("*정보") || message.startsWith("*도움") ||
				message.equalsIgnoreCase(this.getNick()) || message.startsWith("*?")){
			if(Util.isSame(getNick(), "코코나") && Util.isSame(sender, Constant.ADMIN_NICK)){
//				sendMessage(channel, "낫쨩… 따라갈래…");
//				sendMessage(channel, "에고 서치중…");
			}
			else{
				sendMessage(channel, getNick() + " : " + Util.noCall(Constant.ADMIN_NICK)
						+ "이 개발중인 봇입니다. (舊 : SonicCat, SCV, CELICA)");
				sendMessage(channel, "명령 접두어로는 *를 사용합니다. 명령어 목록은 *명령을 사용해주세요.");
//				sendMessage(channel, "또한 기능 제안은 " + Util.noCall(Constant.ADMIN_NICK) + "에게 쿼리로 넣어주시기 바랍니다.");
			}
		}
		
		
		/** Module Attachment Part */
		sendBreakMessage(channel, OrderManager.message(channel, sender, content));

		if(message.contains(":") && (message.contains("-") || message.contains("+"))){
			sendMessage(channel, TimeOper.operate(message));
		}
		
		if(message.startsWith("*명령") || message.startsWith("*order")){ orderList(channel); }
		if(message.startsWith("*거울") || message.startsWith("*대칭")){ reflect(channel, message.substring(3)); }
		if(message.startsWith("*refl")){ reflect(channel, message.substring(5)); }
		if(message.startsWith("*위키 ")){ wiki(channel, message.substring(4)); }
		if(message.startsWith("*wiki ")){ wiki(channel, message.substring(6)); }
		if(message.startsWith("*주인?")){ sendMessage(channel, "주인 : " + Util.noCall(Constant.ADMIN_NICK)); }
		if(message.startsWith("*주인 트위터")){ sendMessage(channel, "https://twitter.com/TOZ57KAIST"); }
		
		// RGinfo call
		sendBreakMessage(channel, RGI.process(content));
		
		/** Byte 입력된걸 단위 바꿔주기 -------------------------------------------------- */
		if(message.startsWith("*바이트") || message.toLowerCase().startsWith("*byte")){
			if(message.contains("?")){
				sendMessage(channel, "주어진 바이트를 KB, MB, GB, TB, PB, EB 단위로 환산합니다.(최대값 : 8EB)");
			}
			long byteSize = Long.parseLong(message.substring(message.indexOf(' ')+1));
			double calc = byteSize;
			int prefixLevel = 0;
			String[] prefix = {"", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei"};
			while(calc >= 1024){ calc /= 1024; prefixLevel++; System.out.println(" -> " + calc); }
			calc = (double)Math.round((calc * 100)) / 100; // rounding
			sendMessage(channel, "" + calc + ""+ prefix[prefixLevel] + "B");
		}
		/** -------------------------------------------------- */
		if(message.startsWith("*패치")){
			sendBreakMessage(channel, "~ 패치내역 ~\n" + patch);
		}
		if(message.startsWith("*버전") || message.toLowerCase().startsWith("*ver")){
			sendMessage(channel, "버전 : " + Constant.BOLD + "" + patchDate + "" + Constant.BOLD);
		}
		if(message.startsWith("*개시") || message.toLowerCase().startsWith("*start")){
			sendMessage(channel, INIT_TIME.toString());
		}
		if(message.startsWith("*작동") || message.toLowerCase().startsWith("*run")){
			long sec = (Calendar.getInstance().getTimeInMillis()-INIT_TIME.getTime())/1000;
			long min = sec/60; sec %= 60;
			long hour = min/60; min %= 60;
			long day = hour/24; hour %= 24;
			String msg = "Running during";
			if(day != 0){ msg += " " + day + "d"; }
			if(hour != 0){ msg += " " + hour + "h"; }
			if(min != 0){ msg += " " + min + "m"; }
			if(sec != 0){ msg += " " + sec + "s"; }
			sendMessage(channel, msg);
		}
		if(message.startsWith("*액션") || message.toLowerCase().startsWith("*action")){
			String action = message.substring(message.indexOf(' ') + 1);
			sendAction(channel, action);
		}
		if(message.startsWith("*cc ")){ caesar(channel, message.substring(4)); } // Caesar cipher
		if(message.startsWith("*cca ")){ caesarAll(channel, message.substring(5)); } // cc all
		if(message.startsWith("*접속 ")){ String channelName = message.substring(4); joinC(channelName); }
		if(message.startsWith("*join ")){ String channelName = message.substring(6); joinC(channelName); }
		if(message.startsWith("*오일러 ")){ int q = Integer.parseInt(message.substring(5)); PE(channel, q); }
		if(message.startsWith("*PE ")){ int q = Integer.parseInt(message.substring(4)); PE(channel, q); }
		if(message.startsWith("*미러 ")){ String word = message.substring(4); namuMirror(channel, word); }
		if(message.startsWith("*나무 ")){ String word = message.substring(4); namu(channel, word); }
		if(message.startsWith("*namu ")){ String word = message.substring(6); namu(channel, word); }
		if(message.startsWith("*레딧 ")){ String word = message.substring(4); reddit(channel, word); }
		if(message.startsWith("*reddit ")){ String word = message.substring(8); reddit(channel, word); }
		if(message.startsWith("*주사위 ")){ dice(channel, message.substring(5)); }
		if(message.startsWith("*dice ")){ dice(channel, message.substring(6)); }
		if(message.startsWith("*나가") || message.startsWith("*ㄲㅈ") || message.startsWith("*leave")){
			if(Constant.ADMINmode){
				if(Util.isAdmin(login)){ partChannel(channel, "Leaving Channel..."); }
				else{ sendMessage(channel, sender + ", ㅗ"); }
			}
			else{ partChannel(channel, sender + "가 나가래요 ㅇㅇ....."); }
		}
		/** @deprecated parts
		// RailTime Opers
		String s = new RailTimeOld().process(Util.parse(message.toLowerCase()));
//		System.out.println(s);
		if(s != null){ sendBreakMessage(channel, s); }
		*/
		
		// Constant.snq.process에서 답을 인식했는지도 확인 + 뒤에 있는 명령을 씹어먹어서 ㅡㅡ
	}

	public void sendBreakMessage(String channel, String message){
		while(true){
			int pos = message.indexOf("\n");
			if(pos==-1){ sendMessage(channel, message); break; }
			else{
				sendMessage(channel, message.substring(0,pos));
				message = message.substring(pos+1);
			}
		}
	}
	
	/** -------------------------------------------------- */
	public void orderList(String channel){
		sendBreakMessage(channel, OrderList.getOrders());
		if(channel.contains("#MeltCraft")){
			sendMessage(channel, "**, *p, *P, *ㅔ : !players(MeltCraft 내 사람 정보 출력) : #MeltCraft 한정");
		}
	}
	public void reflect(String channel, String message){
		String newMsg = "";
		if(message.startsWith(" ")){
			message = message.substring(1);
			while(!message.isEmpty()){
				newMsg = message.charAt(0) + newMsg;
				message = message.substring(1);
			}
			sendMessage(channel, "대칭된 메시지 : " + newMsg);
		}
		else{
			sendMessage(channel, "대칭시킬 메시지를 입력해주시기 바랍니다.");
		}
	}
	public void caesar(String channel, String message){
		int coord = message.indexOf(" ");
		int unit;
		try{ unit = Integer.parseInt(message.substring(0, coord)); }
		catch(NumberFormatException e){
			sendMessage(channel, "입력이 잘못되었습니다. 올바른 입력 예시 : *cc 5 Message"); return;
		}
		String msg = message.substring(coord+1);
		String pw = "";
		unit %= 26;
		if(unit < 0){ unit += 26; }
		for(int i=0;i<msg.length();i++){
			int pre = (int)msg.charAt(i);
			if(pre >= 65 && pre <= 90){ pw += (char)((pre - 65 + unit) % 26 + 65); }
			else if(pre >= 97 && pre <= 122){ pw += (char)((pre - 97 + unit) % 26 + 97); }
			else{ pw += (char)pre; }
		}
		sendMessage(channel, "암호화된 메시지 : " + pw);
	}
	public void caesarAll(String channel, String message){
		String pw = "";
		for(int unit=1;unit<26;unit++){
			for(int i=0;i<message.length();i++){
				int pre = (int)message.charAt(i);
				if(pre >= 65 && pre <= 90){ pw += (char)((pre - 65 + unit) % 26 + 65); }
				else if(pre >= 97 && pre <= 122){ pw += (char)((pre - 97 + unit) % 26 + 97); }
				else{ pw += (char)pre; }
			}
			pw += "(" + unit + ")";
			if(unit % 5 == 0){ sendMessage(channel, "결과 : " + pw); pw = ""; }
			else{ pw += " | "; }
		}
	}
	public void joinC(String channelName){
		if(channelName.charAt(0) != '#'){ channelName = "#" + channelName; }
		joinChannel(channelName);
	}
	public void PE(String channel, int q){
		if(q <= 0){ sendMessage(channel, "사용법 : *PE (문제번호) or *오일러 (문제번호)"); }
		else{ sendMessage(channel, "http://projecteuler.net/problem=" + q); }
	}
	public void wiki(String channel, String message){
		if(message.charAt(2) == ' '){
			String lang = message.substring(0, 2);
			String search = message.substring(3);
			sendMessage(channel, "http://" + lang + ".wikipedia.org/wiki/" + search);
		}
		else{
			sendMessage(channel, "사용법 : *위키(wiki) (언어명) (검색어)");
			sendMessage(channel, "예시 : *위키 ko 수학 / *wiki en Computer Science");
		}
	}
	public void dice(String channel, String message){
		int comma = message.indexOf(",");
		int max = 1; int trial = 1;
		if(comma == -1){ max = Integer.parseInt(message); } // no comma;
		else{
			max = Integer.parseInt(message.substring(0, comma));
			trial = Integer.parseInt(message.substring(comma+1));
		} // else : more than one comma
		if(max > 100 || trial > 100){
			sendMessage(channel, "주사위 명령에서의 입력값은 100을 넘을 수 없습니다!");
			return;
		}
		String answer = ""; 
		for(int i=0;i<trial;i++){
			Random r = new Random();
			answer += (r.nextInt(max)+1) + " | ";
		}
		sendMessage(channel, answer.substring(0, answer.length() - 2));
	}
	public void namu(String channel, String word){
		word = word.replace(" ", "%20");
		sendMessage(channel, "https://namu.wiki/w/" + word);
	}
	public void reddit(String channel, String word){
		word = word.replace(" ", "%20");
		sendMessage(channel, "http://reddit.com/r/" + word);
	}
	public void namuMirror(String channel, String word){
		word = word.replace(" ", "%20");
		sendMessage(channel, "http://namu.mirror.wiki/wiki/" + word);
	}
	/** Blackjack GAME PART ----------------------- */
	
	public int chaserCoord(String sender){
		int coord = -1;
		for(int i=0;i<Constant.ChaserPlay.size();i++){
			if(Util.isSame(Constant.ChaserPlay.get(i).getNick(), sender)){
				coord = i; break;
			}
		}
		return coord;
	}
	public void chaser(String sender, String channel, ArrayList<String> data){
		if(Util.isSame(data.get(0), "*chs") || Util.isSame(data.get(0), "*체") || Util.isSame(data.get(0), ".")){
			String nick = sender;
			if(data.size() > 2){
				nick = data.get(2);
			}
			int coordSender = chaserCoord(sender);
			int coord = chaserCoord(nick);
			
			if(data.size() == 1){
				sendMessage(channel, "체이서 게임 명령입니다. 자세한 것은 *체이서 도움을, 명령어들은 "
						+ "*체이서 명령을 참조하세요.");
			}
			else if(Util.isSame(data.get(1), "도") || Util.isSame(data.get(1), "도움")
					|| Util.isSame(data.get(1), "h") || Util.isSame(data.get(1), "help")){
				String msg = "체이서 게임은 주사위를 굴려서 높은 값을 얻는 게임입니다.\n"
						+ "이때 굴린 결과물에서 몇 개의 주사위를 다시 굴릴 수 있으며,"
						+ " 다시 굴리는 것은 3번까지 가능합니다.\n"
						+ "나온 주사위 눈은 각각의 족보 칸에 넣을 수 있으며, 칸에 따라서 점수가 다르고,"
						+ " 각 칸은 한번씩밖에 사용할 수 없으니 신중하게 고르시기 바랍니다.\n"
						+ "족보 각 칸은 다음과 같습니다.\n"
						+ "CS : 주사위 5개가 같은 눈이면 50점, 아니면 0점.\n"
						+ "St : '12345'이면 40점, 아니면 0점.\n"
						+ "ES : '23456'이면 30점, 아니면 0점.\n"
						+ "FD : 주사위 5개 중 4개가 같은 눈이면 모든 주사위 눈의 합만큼 점수를 얻고, 아니면 0점.\n"
						+ "FH : 풀하우스(AABBB)이면 모든 주사위 눈의 합만큼 점수를 얻고, 아니면 0점.\n"
						+ "Ch : 초이스(투페어, AABBC)이면 모든 주사위 눈의 합만큼 점수를 얻고, 아니면 0점.\n"
						+ "Xs(X=1~6) : (X의 개수)*(X)만큼 점수를 얻는다.";
				sendBreakMessage(channel, msg);
			}
			else if(Util.isSame(data.get(1), "명") || Util.isSame(data.get(1), "명령")
					|| Util.isSame(data.get(1), "o") || Util.isSame(data.get(1), "order")){
				sendBreakMessage(channel, "일단 *체이서 대신 *chs, *체를 사용하여도 같은 기능을 합니다.\n"
						+ "*체이서 시작(시, i, start) + : 체이서 게임을 시작합니다."
						+ " +를 붙일 경우 주사위가 자동 정렬을 하며, 붙이지 않을 경우 자동으로 정렬하지 않습니다.\n"
						+ "*체이서 도움(도, h, help) + : 체이서 게임 도움말입니다."
						+ " 족보 각 칸에 대해서도 배울 수 있습니다.\n"
						+ "*체이서 명령(명, o, order) : 체이서 게임 명령어들을 봅니다.\n"
						+ "*체이서 주사위(주, d, dice) : 현재 주사위 상태를 봅니다.\n"
						+ "*체이서 굴리기(굴, r, roll) <주사위> : 주사위 5개를 굴립니다."
						+ " 뒷부분에는 그대로 놔둘 주사위를 고를 수 있습니다(굴린 이후 굵은 글자로 나옴)\n"
						+ "*체이서 저장(저, s, save) <족보> : 굴려진 주사위 결과를 해당 족보 칸에 저장합니다.\n"
						+ "*체이서 족보(족, t, table) <유저> : <유저>의 현재까지 족보를 확인합니다."
						+ " <유저>가 비어 있으면 자신의 족보를 확인합니다.\n"
						+ "*체이서 점수(점, g, grade) <유저> : <유저>의 현재까지 점수를 확인합니다."
						+ " <유저>가 비어 있으면 자신의 점수를 확인합니다.\n"
						+ "*체이서 모드(모, m, mode) : 자동 정렬 기능을 on/off합니다(on <-> off).\n"
						+ "*체이서 클리어(클, c, clear) : 게임이 완료된 이후 자신의 족보 판을 리셋합니다.");
			}
			else if(Util.isSame(data.get(1), "시") || Util.isSame(data.get(1), "시작")
					|| Util.isSame(data.get(1), "i") || Util.isSame(data.get(1), "start")){
				if(coord != -1){ chaserError2(channel); return; }
				boolean auto = data.size() > 2 && Util.isSame(data.get(2), "+");
				Constant.ChaserPlay.add(new Chaser(sender, auto));
				String ans = "Chaser 게임을 시작합니다! [Player : " + Util.noCall(sender) + "]";
				if(auto){ ans += " (자동정렬 모드)"; }
				sendMessage(channel, ans);
			}
			else if(Util.isSame(data.get(1), "주") || Util.isSame(data.get(1), "주사위")
					|| Util.isSame(data.get(1), "d") || Util.isSame(data.get(1), "dice")){
				if(coord == -1){ chaserError(channel); }
				else{ sendMessage(channel, Constant.ChaserPlay.get(coord).dicePrint()); }
			}
			else if(Util.isSame(data.get(1), "굴") || Util.isSame(data.get(1), "굴리기")
					|| Util.isSame(data.get(1), "r") || Util.isSame(data.get(1), "roll")){
				if(coordSender == -1){ chaserError(channel); return; }
				String ret = "";
				if(data.size() > 2){
					String storage = data.get(2);
					if(Constant.ChaserPlay.get(coordSender).getSave(storage) == null){
						sendMessage(channel, Util.noCall(sender) + ", 남겨둘 주사위가 잘못되었습니다!");
						return;
					}
					else{
						ret = data.get(2);
					}
				}
				sendMessage(channel, Constant.ChaserPlay.get(coordSender).roll(ret));
			}
			else if(Util.isSame(data.get(1), "저") || Util.isSame(data.get(1), "저장")
					|| Util.isSame(data.get(1), "s") || Util.isSame(data.get(1), "save")){
				if(coordSender == -1){ chaserError(channel); return; }
				if(data.size() == 2){ sendMessage(channel, Util.noCall(sender) + ", 어느 칸에 넣을 지 지정해야 합니다!"); }
				else{
					sendMessage(channel, Constant.ChaserPlay.get(coordSender).put(data.get(2)));
				}
			}
			else if(Util.isSame(data.get(1), "족") || Util.isSame(data.get(1), "족보")
					|| Util.isSame(data.get(1), "t") || Util.isSame(data.get(1), "table")){
				if(coord == -1){ chaserError(channel); }
				else{
					String ans = Constant.ChaserPlay.get(coord).printRank();
					sendMessage(channel, ans);
				}
			}
			else if(Util.isSame(data.get(1), "점") || Util.isSame(data.get(1), "점수")
					|| Util.isSame(data.get(1), "g") || Util.isSame(data.get(1), "grade")){
				if(coord == -1){ chaserError(channel); }
				else{
					String ans = Integer.toString(Constant.ChaserPlay.get(coord).score());
					sendMessage(channel, Util.noCall(nick) + " 현재 점수 : " + ans);
				}
			}
			else if(Util.isSame(data.get(1), "모") || Util.isSame(data.get(1), "모드")
					|| Util.isSame(data.get(1), "m") || Util.isSame(data.get(1), "mode")){
				if(coord == -1){ chaserError(channel); }
				else{
					Constant.ChaserPlay.get(coord).autoConvert();
					boolean x = Constant.ChaserPlay.get(coord).auto();
					String ans = "자동정렬을 ";
					if(x){ ans += "off"; } else{ ans += "on"; }
					ans += "에서 ";
					if(x){ ans += "on"; } else{ ans += "off"; }
					ans += "로 변환하였습니다.";
					sendMessage(channel, ans);
				}
			}
			else if(Util.isSame(data.get(1), "클") || Util.isSame(data.get(1), "클리어")
					|| Util.isSame(data.get(1), "c") || Util.isSame(data.get(1), "clear")){
				if(coord == -1){ chaserError(channel); }
				else{
					sendMessage(channel, Util.noCall(sender) + " 보드가 리셋되었습니다.");
					boolean x = Constant.ChaserPlay.get(coord).auto();
					Constant.ChaserPlay.remove(coord);
					Constant.ChaserPlay.add(new Chaser(sender, x));
				}
			}
		}
	}
	public void chaserError(String channel){
		sendMessage(channel, "현재 Chaser를 플레이중이지 않습니다!");
	}
	public void chaserError2(String channel){
		sendMessage(channel, "이미 Chaser를 플레이중입니다!");
	}
}