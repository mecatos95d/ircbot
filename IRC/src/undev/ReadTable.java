package undev;
// Reading current table
import java.net.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.io.*;

import util.*;

public class ReadTable {
	public static String entry(String team, boolean alter, boolean coach) throws Exception {
		String coord = "http://www.koreabaseball.com/News/PlayerRegist1.aspx";
		if(alter){ coord += "?gDate=" + date(); }
		URL page = new URL(coord);
		BufferedReader in = new BufferedReader(new InputStreamReader(page.openStream()));

		team = getShortName(team);
		if(Util.isSame(team, "HELP")){ return help(); }
		String inputLine;
    	String[] remove = {"\t", " ", "<thscope=\"col\">", "</th>", "<liclass=\"first\">", "<li>", "</li>"};
		while(true){ // find table
			inputLine = in.readLine();
			if(inputLine == null){ break; }
			if(inputLine.contains("일자별 등록명단")){ break; }
		}
		ArrayList<String> parts = new ArrayList<String>();
		boolean on = false; 
		while(true){ // read parts
			inputLine = in.readLine();
			if(inputLine == null){ break; }
			if(inputLine.contains("<thead>")){ on = true; }
			if(on && inputLine.contains("</th>")){
				parts.add(getContent(inputLine, remove));
			}
			if(inputLine.contains("</thead>")){ break; }
		}
		while(true){ // finding team
			inputLine = in.readLine();
			if(inputLine == null){ in.close();
				return "팀 이름을 해석할 수 없습니다. 팀 이름은 " + Constant.BOLD
						+ "롯데 한화 삼성 넥센 두산 NC KIA LG SK" + Constant.BOLD + " 중 하나를 입력해주세요\n."
						+ "자세한 도움말은 " + Constant.BOLD + "*엔트리 도움말" + Constant.BOLD + "를 이용하세요.";
			}
			if(inputLine.contains(team)){ break; }
		}
		ArrayList<String>[] players = new ArrayList[parts.size()-1];
		for(int i=0;i<parts.size()-1;i++){
			players[i] = new ArrayList<String>();
			on = false;
			while(true){ // read players
				inputLine = in.readLine();
				if(inputLine == null){ break; }
				if(inputLine.contains("<ul>")){ on = true; }
				if(on && inputLine.contains("</li>")){
					players[i].add(getContent(inputLine, remove));
				}
				if(inputLine.contains("</ul>")){ break; }
			}
		}
		in.close();
		if(players[0].size() == 0){ // 감독이 없을 경우 = 오늘자 데이터가 없을경우(어제 데이터에서 뽑아옴)
			return entry(team, true, coach);
		}
		String x = Constant.BOLD + getFullName(team) + Constant.BOLD + " 현재 1군 엔트리";
		if(!coach){ x += "(선수단)"; }
		for(int i=0;i<parts.size()-1;i++){
			if(!coach && (Util.isSame(parts.get(i+1), "감독") || Util.isSame(parts.get(i+1), "코치") )){ continue; }
			x += "\n" + Constant.BOLD + parts.get(i+1) + Constant.BOLD + " | " + print(players[i]);
		}
		return x;
	}
	public static String getContent(String input, String[] remove){
		for(int i=0;i<remove.length;i++){
			input = input.replace(remove[i], "");
		}
		return input;
	}
	public static String print(ArrayList<String> input){
		String x = "";
		for(int i=0;i<input.size();i++){
			if(i != 0){ x += " "; }
			x += input.get(i);
		}
		return x;
	}
	public static String getShortName(String team){
		team = team.toLowerCase();
		String[][] names = {
				{"help", "도움말", "사용법", "use"},
				{"롯데", "롯데 자이언츠", "자이언츠", "lotte", "lotte giants", "giants", "꼴데"},
				{"한화", "한화 이글스", "이글스", "hanwha", "hanwha eagles", "eagles", "꼴칰", "칰"},
				{"삼성", "삼성 라이온즈", "라이온스", "samsung", "samsung lions", "lions"},
				{"넥센", "넥센 히어로즈", "히어로즈", "nexen", "nexen heroes", "heroes"},
				{"두산", "두산 베어스", "베어스", "doosan", "doosan bears", "bears", "돡"},
				{"nc", "NC 다이노스", "다이노스", "엔씨", "nc dinos", "dinos"},
				{"kia", "kia 타이거즈", "타이거즈", "기아", "kia tigers", "tigers"},
				{"lg", "lg 트윈스", "트윈스", "엘지", "lg twins", "twins", "엘쥐", "쥐", "꼴쥐", "7쥐", "헬쥐"},
				{"sk", "sk 와이번즈", "와이번즈", "스크", "sk wyverns", "wyverns", "슼", "쇀", "솩"}};
		for(int i=0;i<names.length;i++){
			for(int j=0;j<names[i].length;j++){
				if(Util.isSame(team, names[i][j])){ return names[i][0].toUpperCase(); }
			}
		}
		return "asdf";
	}
	public static String getFullName(String team){
		if(Util.isSame(team, "롯데")){ return "롯데 자이언츠"; }
		else if(Util.isSame(team, "한화")){ return "한화 이글스"; }
		else if(Util.isSame(team, "삼성")){ return "삼성 라이온즈"; }
		else if(Util.isSame(team, "넥센")){ return "넥센 히어로즈"; }
		else if(Util.isSame(team, "두산")){ return "두산 베어스"; }
		else if(Util.isSame(team, "NC")){ return "NC 다이노스"; }
		else if(Util.isSame(team, "KIA")){ return "KIA 타이거즈"; }
		else if(Util.isSame(team, "LG")){ return "LG 트윈스"; }
		else if(Util.isSame(team, "SK")){ return "SK 와이번즈"; }
		return "-";
	}
	public static String help(){
		final String help = "현재 한국프로야구 각 팀의 1군 등록 엔트리를 출력해주는 명령입니다.\n"
				+ Constant.BOLD + "*엔트리 <팀>" + Constant.BOLD + "을 입력 시 엔트리를 확인할 수 있습니다.\n"
				+ Constant.BOLD + "*엔트리+ <팀>" + Constant.BOLD
					+ "을 입력 시 감독/코치까지 확인이 가능합니다.\n"
				+ "현재 한국프로야구 9개 구단 : " + Constant.BOLD
				+ "롯데 한화 삼성 넥센 두산 NC KIA LG SK" + Constant.BOLD + "\n"
				+ "출처 : http://www.koreabaseball.com/News/PlayerRegist1.aspx";
		return help;
	}
	public static String date(){
		Calendar x = Calendar.getInstance();
		x.add(Calendar.DATE, -1); // to get yesterday data(to make alter position - when entry is not uploaded
		String ans = "";
		ans += "" + x.get(Calendar.YEAR);
		ans += to2d(x.get(Calendar.MONTH)+1);
		ans += to2d(x.get(Calendar.DAY_OF_MONTH));
		return ans;
	}
	public static String to2d(int x){
		if(x < 10){ return "0" + x; }
		else{ return "" + x; }
	}
}