package tank;

import java.util.Random;

class Tank{
	int[] shield = new int[3]; // [Front, Side, Back]
	int through;
	int damage;
	int hp;
	int hide;
	int rotate;
	int speed;
	
	Tank(){
		Random r = new Random();
		shield[0] = 1+r.nextInt(1000);
		through = 1+r.nextInt(1000);
	}
}