package struct;

import java.util.ArrayList;

import util.Constant;
import util.Util;

public abstract class Order{
	private static String HELP;
	public static String[] ORDER;
	private static String ERROR;
	
	public Order(String help, String[] order){
		HELP = help;
		ORDER = order;
		ERROR = "";
	}
	public Order(String help, String[] order, String error){
		HELP = help;
		ORDER = order;
		ERROR = error;
	}
	
	// In here, code is unused. Code is used at OrderOption.
	public String message(String sender, ArrayList<String> content, int code) throws Exception{
		System.out.println("FUNCTION CALLED IN CLASS ORDER");
		return sender;
	}

	public static String help(){
		return HELP;
	}
	
	/** When checking order without star */
	public static int codeWithoutStar(String msg){
		msg = msg.toLowerCase();
		for(int i=0;i<ORDER.length;i++){
			if(Util.isSame(msg, ORDER[i])){
				return 0;
			}
		}
		return -1; // Not msg
	}
	public int code(String msg){
		msg = msg.toLowerCase();
		for(int i=0;i<ORDER.length;i++){
			if(Util.isSame(msg, "*" + ORDER[i])){
				return 0;
			}
		}
		return -1; // Not msg
	}
}
