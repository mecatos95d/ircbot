package struct;

import java.util.ArrayList;

import struct.OrderCode;
import util.Util;

public class ClassSample{
	private final static String HELP = ""; // HELP message
	public final static String[] order = {}; // ORDER Message List : First reaction word. order[0] = Korean Origin / order[1] = English Origin.
	public final static String[] orderWS = {}; // OPTIONAL - Order Without Star. Use OrderCode.codeWithoutStart
	private final static String[][] option = {}; // OPTION Message List : addon reaction word
	
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order, option);
		if(c == -1){ return ""; }

		if(content.size() < 2){ return HELP; } // When Not Enough input parameters
		if(Util.isSame(content.get(1), "?")){ return HELP; } // When "*order ?" case.
		
		// Message Reaction Part. Return Message. If not, return ""
		return "";
	}
}