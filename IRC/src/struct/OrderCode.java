package struct;

import util.Util;

public class OrderCode{
	/** When checking order without star */
	public static int codeWithoutStar(String order, String[] orderList){
		order = order.toLowerCase();
		for(int i=0;i<orderList.length;i++){
			if(Util.isSame(order, orderList[i])){
				return 0;
			}
		}
		return -1; // Not order
	}
	public static int code(String order, String[] orderList){
		order = order.toLowerCase();
		for(int i=0;i<orderList.length;i++){
			if(Util.isSame(order, "*" + orderList[i])){
				return 0;
			}
		}
		return -1; // Not order
	}
	/** When option is attached as addon */
	public static int code(String order, String[] orderList, String[][] optionList){
		order = order.toLowerCase();
		for(int i=0;i<orderList.length;i++){
			for(int j=0;j<optionList.length;j++){
				for(int k=0;k<optionList[j].length;k++){
					if(Util.isSame(order, "*" + orderList[i] + optionList[j][k])){
						return j;
					}
				}
			}
		}
		return -1; // Not order.
	}
	/** When option is another word */
	public static int code(String order, String option, String[] orderList, String[][] optionList){
		int c = code(order, orderList);
		if(c == -1){ return -1; }
		else{
			option = option.toLowerCase();
			for(int i=0;i<optionList.length;i++){
				for(int j=0;j<optionList[i].length;j++){
					if(Util.isSame(option, optionList[i][j])){
						return i;
					}
				}
			}
		}
		return -2; // Proper Order, Not Order
	}
}
