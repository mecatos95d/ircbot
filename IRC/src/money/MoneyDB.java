/** @Deprecated */

package money;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import util.*;

public class MoneyDB{ // Database of Money
	final String currencyFileName = "./data/economy/currency.txt";
	final String moneyFileName = "./data/economy/money.txt";
	public ArrayList<Money> moneyDB = new ArrayList<Money>();
	int moneyPerMineral;
	int moneyPerGas;
	public MoneyDB() throws FileNotFoundException{
		Scanner currencyInput = new Scanner(new File(currencyFileName));
		moneyPerMineral = currencyInput.nextInt();
		moneyPerGas = currencyInput.nextInt();
		currencyInput.close();
		Scanner moneyInput = new Scanner(new File(moneyFileName));
		while(moneyInput.hasNext()){
			String line = moneyInput.nextLine();
			int space = line.indexOf(' ');
			moneyDB.add(new Money(line.substring(0, space), Integer.parseInt(line.substring(space+1))));
		}
		moneyInput.close();
	}
	void clear0(){ // money가 0인것 제거(출력 조절위함)
		int cur = 0;
		while(cur < moneyDB.size()){
			if(moneyDB.get(cur).getBalance() == 0){ moneyDB.remove(cur); }
			else{ cur++; }
		}
	}
	public void sort(){
		clear0();
		for(int i=1;i<moneyDB.size();i++){
			for(int j=0;j<i;j++){
				if(moneyDB.get(i).getBalance() > moneyDB.get(j).getBalance()){
					Money tmp = moneyDB.get(i);
					moneyDB.set(i, moneyDB.get(j));
					moneyDB.set(j, tmp);
				}
			}
		}
		saveDB();
	}
	int userCoord(String name){
		for(int i=0;i<moneyDB.size();i++){
			if(Util.isSame(moneyDB.get(i).name, name)){
				return i;
			}
		}
		moneyDB.add(new Money(name));
		return moneyDB.size()-1;
	}
	// 미네랄/가스 -> 돈 환전, amount는 미네랄/가스의 양, isGas가 true면 가스를, false면 미네랄을 환전
	public String currencyAdd(String name, int amount, boolean isGas){ 
		int coord = userCoord(name);
		if(isGas){ amount *= moneyPerGas; }
		else{ amount *= moneyPerMineral; }
		moneyDB.get(coord).add(amount);
		/** 추후 환전량에 따른 환율 변동도 구현할 예정 */
		saveDB();
		return "" + amount + "만큼의 돈으로 환전하였습니다.";
	}
	public void add(String name, int amount){
		moneyDB.get(userCoord(name)).add(amount);
		saveDB();
	}
	public void remove(String name, int amount){
		moneyDB.get(userCoord(name)).remove(amount);
		saveDB();
	}
	public int getBalance(String name){
		return moneyDB.get(userCoord(name)).getBalance();
	}
	public String balanceCheck(String name){
		return Util.noCall(name) + "의 현재 잔고 : " + getBalance(name);
	}
	void saveDB(){
		try {
			PrintWriter outputStream = new PrintWriter(moneyFileName);
			for(int i=0;i<moneyDB.size();i++){
				Money temp = moneyDB.get(i);
				outputStream.println(temp.name + " " + temp.balance);
			}
			outputStream.close();
		} catch (FileNotFoundException e){}
	}
}