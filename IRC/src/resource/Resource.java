/** @Deprecated */

package resource;
import java.util.ArrayList;
import java.util.Random;
import java.util.Date;

import util.*;

class Resource {
	String name;
	int mineral;
	int gas;
	Resource(){}
	Resource(String name){
		this.name = name; mineral = 0; gas = 0;
	}
	int amount(){
		return mineral + gas;
	}
	void addMineral(int amount){ mineral += amount; }
	void addGas(int amount){ gas += amount; }
}

class ChanResource extends Resource{ // name on ChanResource means channel name.
	int mineral_num; int mineral_size; // mineral은 총량, mineral_num은 남은 덩이, mineral_size는 각 덩이의 최대값.
	final long COOLTIME = 60 * 1000; // ms
	Date time = new Date();
	// 명령을 최근에 실행한 시간. 쿨타임 1분(60000)을 체크하기 위함.
	ArrayList<Resource> own = new ArrayList<Resource>();
	ChanResource(String input){
		name = input; mineral = 10000; mineral_num = 5; mineral_size = 2000; gas = 5000; time.setTime(0);
	}
	void sortOwn(){
		Resource temp = new Resource(); // for switching.
		for(int i=1;i<own.size();i++){
			for(int j=0;j<i;j++){
				if(own.get(i).amount() > own.get(j).amount()){
					temp = own.get(i); own.set(i, own.get(j)); own.set(j, temp);
				}
			}
		}
	}
	int userCoord(String name){
		for(int i=0;i<own.size();i++){
			if(Util.isSame(own.get(i).name, name)){
				return i;
			}
		}
		own.add(new Resource(name));
		return own.size()-1;
	}
	int returnMineral(String sender){
		return own.get(userCoord(sender)).mineral;
	}
	String removeMineral(String sender, int amount){
		int owned = returnMineral(sender);
		if(amount > owned){ return "보유 미네랄이 부족합니다! 현재 보유량 : " + owned; }
		else{
			own.get(userCoord(sender)).mineral -= amount;
			return "" + amount + "미네랄을 "; // 뒤에 다른 문장이 class Money에서 붙을 예정
		}
	}
	String getMineral(String sender){
		String reply = "";
		Random r = new Random();
		int cur_dummy = mineral-mineral_size*(mineral_num-1); // 현재 캐고 있는 덩이에 남아있는양
		int amount = Math.min(r.nextInt(100), cur_dummy);
		long time_cur = new Date().getTime(); // current time
		long time_gap = time_cur - time.getTime();
		if(time_gap < COOLTIME){ return "자원을 다시 캐기 위해 " +
				(int)((COOLTIME - time_gap) / 1000) + "초 더 기다려야 합니다."; }
		else if(mineral == 0){ reply = "미네랄이 고갈되었습니다."; }
		else{
			mineral -= amount;	cur_dummy -= amount;
			reply = "지잉";
			for(int i=0;i<r.nextInt(10);i++){ reply += "-지잉"; }
			reply += "\n";
			if(amount < 5){ reply += ""; }
			reply += amount + "미네랄";
			if(amount < 5){ reply += ""; }
			reply += "을 들고 " + Util.noCall(sender) + "에게 돌아갑니다. ";
			reply += name + "의 미네랄 덩이에는 " + cur_dummy + "만큼 남아있습니다.";
			if(mineral_num != 1){
				reply += "(잔여 총량 : " + mineral + ")";
				if(cur_dummy == 0){
					reply += "\n미네랄 덩이가 다 떨어져 새 덩이를 캐기 시작합니다.";
					mineral_num--;
				}
			}
			time.setTime(time_cur);
			own.get(userCoord(sender)).addMineral(amount);
		}
		return reply;
	}
	int returnGas(String sender){
		return own.get(userCoord(sender)).gas;
	}
	String removeGas(String sender, int amount){
		int owned = returnGas(sender);
		if(amount > owned){ return "보유 가스가 부족합니다! 현재 보유량 : " + owned; }
		else{
			own.get(userCoord(sender)).gas -= amount;
			return "" + amount + "가스를 "; // 뒤에 다른 문장이 class Money에서 붙을 예정
		}
	}
	String getGas(String sender){
		String reply = "";
		Random r = new Random();
		int amount = Math.min(r.nextInt(100), gas);
		long time_cur = new Date().getTime(); // current time
		long time_gap = time_cur - time.getTime();
		if(time_gap < COOLTIME){ return "자원을 다시 캐기 위해 " +
				(int)((COOLTIME - time_gap) / 1000) + "초 더 기다려야 합니다."; }
		else if(gas == 0){
			reply = "가스가 고갈되었습니다. 2가스를 들고 "+Util.noCall(sender)+"에게 돌아갑니다.";
			time.setTime(time_cur);
			own.get(userCoord(sender)).addGas(2);
		}
		else{
			gas -= amount;
			if(amount < 5){ reply += ""; }
			reply += amount + "가스";
			if(amount < 5){ reply += ""; }
			reply += "을 들고 " + Util.noCall(sender) + "에게 돌아갑니다. ";
			reply += name + "의 가스 간헐천에는 " + gas + "만큼 남아있습니다.";
			time.setTime(time_cur);
			own.get(userCoord(sender)).addGas(amount);
		}
		return reply;
	}
	String renew(){
		Random r = new Random();
		String reply = name + "에서 새 멀티를 찾아 나섭니다.\n";
		mineral_num = r.nextInt(7)+1; mineral_size = 1000 + r.nextInt(2000);
		mineral = mineral_num * mineral_size; gas = 2000 + r.nextInt(4000);
		reply += "" + mineral_size + "의 미네랄 " + mineral_num + "덩이(총 " + mineral + ")과 "
				+ gas + "의 가스를 발견했습니다. 앞으로는 이 곳에서 자원을 채취하게 됩니다.";
		return reply;
	}
	String status(){
		return "현재 " + name + "에는 " + mineral + "의 미네랄과 " + gas + "의 가스가 남아있습니다.";
	}
	String own(){
		sortOwn();
		String reply = "";
		int max = Math.min(10, own.size());
		for(int i=0;i<max;i++){
			Resource temp = own.get(i);
			reply += Util.noCall(temp.name) + "(" + temp.mineral + "/" + temp.gas + ")";
			if(i != max-1){ reply += " | "; }
		}
		if(own.size() > 10){
			reply += "\n이 외에도 " + (own.size()-10) + "명이 자원을 소지하고 있습니다.";
		}
		return reply;
	}
}