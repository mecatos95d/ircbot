/** @Deprecated */

package resource;

import java.util.ArrayList;

import util.*;

public class ResourceStatus{
	ArrayList<ChanResource> resource = new ArrayList<ChanResource>();
	int resourceCoord(String channel){
		for(int i=0;i<resource.size();i++){
			if(Util.isSame(resource.get(i).name, channel)){
				return i;
			}
		}
		resource.add(new ChanResource(channel));
		return resource.size()-1;
	}
	public String mineral(String channel, String sender){
		return resource.get(resourceCoord(channel)).getMineral(sender);
	}
	public int getMineral(String channel, String sender){
		return resource.get(resourceCoord(channel)).returnMineral(sender);
	}
	public String removeMineral(String channel, String sender, int amount){
		return resource.get(resourceCoord(channel)).removeMineral(sender, amount);
	}
	public String gas(String channel, String sender){
		return resource.get(resourceCoord(channel)).getGas(sender);
	}
	public int getGas(String channel, String sender){
		return resource.get(resourceCoord(channel)).returnGas(sender);
	}
	public String removeGas(String channel, String sender, int amount){
		return resource.get(resourceCoord(channel)).removeGas(sender, amount);
	}
	public String multi(String channel){
		return resource.get(resourceCoord(channel)).renew();
	}
	public String status(String channel){
		return resource.get(resourceCoord(channel)).status();
	}
	public String own(String channel){
		return channel + "의 자원 소지 현황 : " + resource.get(resourceCoord(channel)).own();
	}
}