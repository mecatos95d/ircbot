/** @Deprecated */

package util;
// Reading current table
import java.util.ArrayList;
import java.io.*;

import util.*;

class JubData{
	int[] data = new int[4]; // {유빌리티*100, 난이도, 스코어, 랭크}
	int code;
	JubData(){
		for(int i=0;i<4;i++){
			data[i] = 0;
		}
		code = 0;
	}
	int add(int input){
		int value = input / 10;
		int place = input % 10;
		if(place < 4){
			if(data[place] == 0){ // when unsaved = correct case
				data[place] = value;
				code += Math.pow(2, place);
				return 0;
			}
			else{ return -5; } // 중복입력
		}
		else{ return -6; } // 데이터 에러
	}
	void preProcess(){ // 연산에 도움되도록 기초연산
		
	}
	String result(){
		switch(code){
		case 7: return getNewJub();
		case 6: return stableJubS();
		case 10: return stableJubR();
		case 3: return scoreToStable();
		case 1: return scoreToStableList();
		case 4: return printRank();
		case 8: return printRankScore();
		default: return Jubility.error(-7); // 입력조합미스
		}
	}
	String printJub(int jubility, boolean option){ // option true -> attach sign
		String below = "" + Math.abs(jubility % 100);
		if(below.length() == 1){ below = "0" + below; }
		String ans = jubility/100 + "." + below;
		if(option && jubility > 0){ ans = "+" + ans; }
		if(option && jubility < 0){ ans = "-" + ans; }
		return ans;
	}
	String getNewJub(){
		int newJub;
		double stable = stableJub(data[2]);
		if(stable > data[0]){ newJub = (int)(data[0]*0.8 + stable*0.2); }
		else if(data[1] == 10 || data[2] < 980000){ newJub = (int)(data[0]*0.98 + stable*0.02); }
		else{ newJub = data[0]; }
		return "난이도 " + data[1] + "에서 " + data[2] + "점을 획득 시 유빌리티 변화: "
				+ printJub(data[0], false) + " ⇒ " + printJub(newJub, false)
				+ " (변화량 " + printJub(newJub-data[0], true) + ")";
	}
	String stableJubS(){
		return "난이도 " + data[1] + "에서 " + data[2] + "점을 획득 시 유빌리티 안정값: "
				+ printJub((int)stableJub(data[2]), false);
	}
	String stableJubR(){
		String reply = "난이도 " + data[1] + "에서 " + Jubility.RANKS[data[3]] + "랭크를 획득 시 유빌리티 안정값: "
				+ printJub((int)stableJub(Jubility.RANKCUT[data[3]]), false) + "(" + Jubility.RANKCUT[data[3]] + "점)";
		if(data[3] != 8){ // EXC를 제외하기 위함
			reply += " ~ " + printJub((int)stableJub(Jubility.RANKCUT[data[3]+1]-1), false)
					+ "(" + (Jubility.RANKCUT[data[3]+1]-1) + "점)";
		}
		return reply;
	}
	String scoreToStable(){
		if(data[0] > data[1]*100 + 60){ return "난이도 " + data[1] + "의 최대 유빌리티 안정값은 "
				+ data[1] + ".60 입니다. 입력한 값 : " + printJub(data[0], false); }
		double[] cut = new double[Jubility.CUTLINE.length];
		int score = 0;
		for(int i=0;i<cut.length;i++){
			cut[i] = (double)data[1]*i / 8 + (double)i*(i-5) / 40;
			if(data[0] == cut[i]*100){ score = Jubility.CUTLINE[i]; break; } // to avoid 0 or EXC error case
			if(data[0] < cut[i]*100){
				score = (int)(Jubility.CUTLINE[i-1]
						+ (Jubility.CUTLINE[i]-Jubility.CUTLINE[i-1]) * (data[0]/100.0 - cut[i-1]) / (cut[i] - cut[i-1])); break; 
			}
		}
		return "난이도 " + data[1] + "에서 유빌리티 안정값을 " + printJub(data[0], false) + "로 가지기 위한 점수: "
				+ score + "(" + Jubility.RANKS[getRank(score)] + ")";
	}
	String scoreToStableList(){
		double[] cut = new double[Jubility.CUTLINE.length];
		int score = 0;
		String reply = "유빌리티 안정값을 " + printJub(data[0], false) + "로 가지기 위한 점수:";
		for(int j=Math.max(1, (int)Math.ceil((data[0]-60)/100.0));j<=10;j++){ // j is level
			for(int i=0;i<cut.length;i++){
				cut[i] = (double)j*i / 8 + (double)i*(i-5) / 40;
				if(data[0] == cut[i]*100){ score = Jubility.CUTLINE[i]; break; } // to avoid 0 or EXC error case
				if(data[0] < cut[i]*100){
					score = (int)(Jubility.CUTLINE[i-1]
							+ (Jubility.CUTLINE[i]-Jubility.CUTLINE[i-1]) * (data[0]/100.0 - cut[i-1]) / (cut[i] - cut[i-1])); break; 
				}
			}
			reply += " " + j + ":" + score + "(" + Jubility.RANKS[getRank(score)] + ")";
		}
		return reply;
	}
	String printRank(){
		return data[2] + "점은 " + Jubility.RANKS[getRank(data[2])] + "랭크입니다.";
	}
	String printRankScore(){
		String reply = Jubility.RANKS[data[3]] + "랭크는 " + Jubility.RANKCUT[data[3]] + "점";
		if(data[3] != 8){ // EXC를 제외하기 위함
			reply += " ~ " + (Jubility.RANKCUT[data[3]+1]-1) + "점";
		}
		return reply + "입니다.";
	}
	double stableJub(int score){ // 난이도 입력되어있다고 가정
		int rank = 0;
		for(int i=1;i<Jubility.CUTLINE.length;i++){
			if(score >= Jubility.CUTLINE[i]){ rank++; }
			else{ break; }
		}
		if(rank == 8){ return data[1]*100 + 60; } // Excellent case. to avoid error
		double left = (double)data[1]*rank / 8 + (double)rank*(rank-5) / 40;
		int leftScore = Jubility.CUTLINE[rank];
		double right = (double)data[1]*(rank+1) / 8 + (double)(rank+1)*(rank-4) / 40;
		int rightScore = Jubility.CUTLINE[rank+1];
		double stable = left + (right - left) * (score - leftScore) / (rightScore - leftScore);
		return stable*100; // to re-scale as int-form saved jubility data
	}
	int getRank(int score){
		int rank = 0;
		for(int i=1;i<Jubility.RANKCUT.length;i++){
			if(score >= Jubility.RANKCUT[i]){ rank++; }
			else{ break; }
		}
		return rank;
	}
}
public class Jubility {
	static String[] OPERCODE = {"유", "유빗", "유비트", "ju", "jubeat"};
//	static String[] OPERCODE = {"유", "유빗", "유비트", "유빌", "유빌리티", "ju", "jubeat", "jubility"}; // Old version
	static String[] HELPCODE = {"도움", "도움말", "사용법", "help"};
	static String[] CombiCODE = {"조합", "입력", "combination", "combi", "input", "inputset"};
	static final String[] RANKS = {"E", "D", "C", "B", "A", "S", "SS", "SSS", "EXC"};
	static final int[] CUTLINE = {0, 700000, 750000, 800000, 850000, 900000, 950000, 980000, 1000000};
	static final int[] RANKCUT = {0, 500000, 700000, 800000, 850000, 900000, 950000, 980000, 1000000};
	public static boolean needOper(ArrayList<String> input){
		if(input.size() == 0){ return false; }
		for(int i=0;i<OPERCODE.length;i++){
			if(Util.isSame("*" + OPERCODE[i], input.get(0).toLowerCase())){ return true; }
		}
		return false;
	}
	public static boolean needHelp(String input){
		for(int i=0;i<HELPCODE.length;i++){
			if(Util.isSame(HELPCODE[i], input.toLowerCase())){ return true; }
		}
		return false;
	}
	public static boolean needCombi(String input){
		for(int i=0;i<CombiCODE.length;i++){
			if(Util.isSame(CombiCODE[i], input.toLowerCase())){ return true; }
		}
		return false;
	}
	public static String main(ArrayList<String> input){
		if(input.size() < 2){ return other(); }
		else if(needHelp(input.get(1))){ return help(); }
		else if(needCombi(input.get(1))){ return combi(); }
		else{
			JubData jub = new JubData();
			for(int i=1;i<input.size();i++){
				int data = checkData(input.get(i));
				if(data < 0){ return error(data); } // negatives are error
				int value = jub.add(data);
				if(value < 0){ return error(data); } // negatives are error also
			}
			return jub.result();
		}
	}
	public static int checkRank(String rank){
		for(int i=0;i<RANKS.length;i++){
			if(Util.isSame(RANKS[i], rank)){ return i; }
		}
		return -1;
	}
	public static int checkData(String input){
		/* {유빌리티, 난이도, 스코어, 랭크} 중 어떤것인지 판별. 
		 * 리턴 값은 10*데이터+코드
		 * 유빌리티 = not int, below 10.06, contains '.'
		 * 난이도 = int, 1-10
		 * 스코어 = other case
		 * 랭크 = String
		 */
		if(Util.isStringDouble(input)){
			double data = Double.parseDouble(input);
			if(data <= 0){ return -1; } // 입력값 양수여야만함
			else if(data <= 10.6){
				if(input.contains(".")){ // jubility. xx.yy -> xxyy0
					return (int)(1000*data);
				}
				else if(Util.isInt(data)){ // hardness
					return (int)data*10+1;
				}
			}
			else{ // score
				if(input.contains(".") || data <= 100){ // score in xx.xxxx
					if(data > 100){ return -2; } // 100.0 초과
					return (int)(100000*data)+2;
				}
				else if(Util.isInt(data)){ // normal 6-digit case
					if(data > 1000000){ return -3; } // 100만점 초과
					return (int)data*10+2;
				}
			}
		}
		else{ // when string = rank
			int rank = checkRank(input);
			if(rank == -1){ return -4; } // 랭크입력에러
			else{ return rank*10+3; }
		}
		return 4; // error case - 입력이 잘못됨
	}
	public static String error(int code){
		switch(code){
		case -1: return "입력값은 양수 또는 랭크만 가능합니다!";
		case -2: return "유비트 최대 점수는 100.0000(100만점)입니다!";
		case -3: return "유비트 최대 점수는 1000000(100만점)입니다!";
		case -4: return "랭크 입력이 잘못되었습니다! 유비트 랭크는 E, D, C, B, A, S, SS, SSS, EXC가 있습니다.";
		case -5: return "입력이 잘못되었습니다! (중복된 입력입니다)";
		case -6: return "입력이 잘못되었습니다!";
		case -7: return "입력이 잘못되었습니다. 올바른 입력 조합 : " + pair();
		}
		return "";
	}
	public static String other(){
		final String other = "jubeat(유비트) 관련 명령입니다. jubility 계산과 점수별 랭킹을 확인 가능합니다. 자세한 것은 "
				+ Constant.BOLD + "*유비트 도움" + Constant.BOLD + "을 참조하시기 바랍니다.";
		return other;
	}
	public static String pair(){
		return "(유빌리티와 난이도와 스코어), (난이도와 유빌리티), (난이도와 스코어), (난이도와 랭크), "
				+ "(유빌리티), (스코어), (랭크)";
	}
	public static String help(){
		final String help = Constant.BOLD + "*ju(jubility), *유(유비트)"
				+ Constant.BOLD + "를 통해서 실행 시킬 수 있습니다.\n"
				+ "명령어 이후에 " + Constant.BOLD + "유빌리티, 난이도, 스코어, 랭크" + Constant.BOLD
					+ " 중 일부를 입력 가능합니다(입력 순서는 상관이 없습니다). "
				+ "이때 스코어 입력 시 (xx.xxxx)꼴의 입력도 인식합니다. 소숫점은 생략해도 무방합니다."
					+ "(단 10.60 이하인 경우는 유빌리티로 인식됩니다)\n"
				+ "단순 정수 입력 시, 유빌리티인지 난이도인지 구분이 불가능하므로 "
					+ "유빌리티 입력시 소숫점을 포함하여 주시기 바랍니다.(예: 6. or 6.0, 6.00)\n";
		return help;
	}
	public static String combi(){
		final String combi = Constant.BOLD + "유빌리티와 난이도와 스코어" + Constant.BOLD + "를 입력 시 "
					+ "변화된 유빌리티를 출력합니다.\n"
				+ Constant.BOLD + "난이도와 스코어 또는 랭크" + Constant.BOLD + "를 입력 시 "
					+ "그 난이도에서 주어진 스코어/랭크를 받을 시의 유빌리티 안정값을 출력합니다.\n"
				+ Constant.BOLD + "난이도와 유빌리티" + Constant.BOLD + "를 입력 시 "
					+ "그 난이도에서 주어진 유빌리티를 안정값으로 가지는 스코어를 출력합니다.\n"
				+ Constant.BOLD + "유빌리티" + Constant.BOLD + "만을 입력 시 "
					+ "그 유빌리티를 안정값으로 가지는 난이도별 점수를 출력합니다.\n"
				+ Constant.BOLD + "스코어" + Constant.BOLD + "만을 입력 시 "
					+ "그 스코어의 랭크를 출력합니다.\n"
				+ Constant.BOLD + "랭크" + Constant.BOLD + "만을 입력 시 "
					+ "그 랭크의 스코어 범위를 출력합니다.\n"
				+ "참고 : http://yosh52.web.fc2.com/jubeat/jubility_copious/matome.html";
		return combi;
	}
}