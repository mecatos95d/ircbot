package util;

import java.util.ArrayList;
import struct.OrderCode;

public class Twitch{
	private final static String HELP = "트위치 채널 명령어입니다. 사용 방법 : *트위치 <채널명>";
	public final static String[] order = {"트위치", "twitch"};
	private final static String[][] nickChan = {
			{"TOZ57", "TÖZ57", "TOZ", "TÖZ"},
			{"ruby3141", "CDnX"},
			{"JiminP"},
			{"Intrainos", "IntrainosIC"}
	};
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order);
		if(c == -1){ return ""; }
		
		boolean needHelp = false;
		String target = "";
		if(content.size() == 1){ target = sender; needHelp = true; }
		else{
			target = content.get(1);
			for(int i=2;i<content.size();i++){
				target += "_" + content.get(i);
			}
		}
		if(Util.isSame(target, "?")){
			return HELP;
		}
		for(int i=0;i<nickChan.length;i++){
			for(int j=0;j<nickChan[i].length;j++){
				if(Util.isSame(target, nickChan[i][j])){
					target = nickChan[i][0];
					needHelp = false;
				}
			}
		}
		if(needHelp){
			return HELP;
		}
		return page(target);
	}
	private static String page(String channel){
		return "http://www.twitch.tv/" + channel;
	}
}
