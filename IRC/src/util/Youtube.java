package util;

import java.util.ArrayList;

import struct.OrderCode;

public class Youtube{
	private final static String HELP = "유튜브 채널 명령어입니다. 사용 방법 : *유튜브 <닉네임> / *유튜브+ <닉네임> (gaming.youtube) | "
			+ "봇에 등록된 사용자만 인식이 가능합니다. 등록은 " + Util.noCall(Constant.ADMIN_NICK) + "에게 문의하세요.";
	private final static String UNAUTH_ERROR = "봇에 등록되지 않은 사용자입니다. 등록은 " + Util.noCall(Constant.ADMIN_NICK) + "에게 문의하세요.";
	public final static String[] order = {"유튜브", "youtube", "유튭", "윹", "y"};
	private final static String[][] option = { {""}, {"+"} };
	
	private final static String[][] nickChan = {
			{"TOZ57", "TÖZ57", "TOZ", "TÖZ"},
			{"ruby", "ruby3141", "CDnX", "엑스"},
			{"책읽는달팽", "Readingsnail", "RS", "책달", "책"}
	};
	private final static String[] page_gaming = {
			"https://gaming.youtube.com/user/ug95/live",
			"https://gaming.youtube.com/channel/UC330_cbsFe2BPvH6vcCJ6cw/live",
			"https://gaming.youtube.com/user/hhgg0614/live"
	};
	private final static String[] page = {
			"http://www.youtube.com/user/ug95/live",
			"https://www.youtube.com/watch?v=Zm9nct1VGhA",
			""
	};
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order, option);
		if(c == -1){ return ""; }

		String target = "";
		int coord = -1;
		if(content.size() == 1){ target = sender; }
		else{
			target = content.get(1);
			for(int i=2;i<content.size();i++){
				target += "_" + content.get(i);
			}
		}
		if(Util.isSame(target, "?")){
			return HELP;
		}
		for(int i=0;i<nickChan.length;i++){
			for(int j=0;j<nickChan[i].length;j++){
				if(Util.isSame(target, nickChan[i][j])){
					coord = i;
				}
			}
		}
		if(coord == -1){
			if(content.size() == 1){ return HELP; }
			else{ return UNAUTH_ERROR; }
		}
		return page(coord, c);
	}
	private static String page(int order, int code){
		if(code == 0){
			if(page[order].length() != 0){ return page[order]; }
		}
		return page_gaming[order];
	}
}