package util.rail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import util.Constant;
import util.Util;
import struct.OrderCode;

public class RailTime{
	private final static String HELP = "열차정보 명령어입니다. 사용 방법 : *열차 <열차번호> [<날짜> <역1> <역2>]"
			+ " | '열차-' 사용시 정차역만, '열차+' 사용시 지연 정보까지 포함. 정차역 입력 시 해당 역, 구간 정보를 출력합니다.";
	public final static String[] order = {"철도", "철도시간", "철도시각", "열차", "열차시간", "열차시각", "rail", "railtime"};
	private final static String[][] option = { {"-"}, {""}, {"+"} };
	
	private final static String PAGE = "http://www.letskorail.com/ebizprd/EbizPrdTicketPr11131_i1.do?";
	private final static String DATE = "txtRunDt=";
	private final static String NUM = "&txtTrnNo=";
	
	public static String message(String sender, ArrayList<String> content) throws Exception{
		int c = OrderCode.code(content.get(0), order, option);
		if(c == -1){ return ""; }

		String NO = ""; // TEST : "1405", First Input Number
		String DT = ""; // TEST : "20161112", Second Input Number
		String STN1 = ""; // First Input Non-Number String
		String STN2 = ""; // Second Input Non-Number String
		
		content.remove(0); // First parameter remove
		
		for(String s : content){
			if(Util.isIntStr(s)){ // String s is int-form.
				if(NO.length() == 0){ NO = s; }
				else if(DT.length() == 0){ DT = s; }
				// else : int input is more than 3.
			}
			else{
				if(STN1.length() == 0){ STN1 = s; }
				else if(STN2.length() == 0){ STN2 = s; }
				// else : int input is more than 3.
			}
		}
		
		if(content.size() == 0 || NO.length() == 0){ return HELP; }
		
		if(DT.length() == 0){ DT = getDate(DT); }
		
		String[] buf = new String[4];
		
		String LINK = PAGE + DATE + DT + NUM + NO;
		
		URL page = new URL(LINK);
		BufferedReader br = new BufferedReader(new InputStreamReader(page.openStream()));
		
		StnList sl = new StnList();
		
		boolean open = false;
		boolean openType = false;
		String typePrint = "";
		int order = 0;
		int i = 0;
		while(true){
			String s = br.readLine();
			if(s == null){ break; }
			if(openType){
				String block = s.trim().replaceAll("</.*>", "").replaceAll("<.*>", "");
				if(block.length() != 0){
					if(typePrint.length() == 0){ typePrint += block + " "; }
					else{ typePrint += block + " | " + LINK + "\n"; openType = false; }
				}
			}
			if(open){
				String block = s.trim().replaceAll("</.*>", "").replaceAll("<.*>", "");
				if(block.length() != 0){
					buf[i] = block;
					i++;
					if(i == buf.length){
						sl.add(new StnTime(buf, order));
						order++;
						i = 0;
					}
				}
			}
			if(s.contains("컬러")){ openType = true; }
			if(s.contains("지연시간")){ open = true; }
		}
		
		int o1 = sl.stnCode(STN1);
		if(o1 == -1){ return "WRONG STATION : " + STN1; }
		int o2 = sl.stnCode(STN2);
		if(o2 == -1){ return "WRONG STATION : " + STN2; }
		
		return typePrint + sl.print(c, o1, o2);
	}
	private static String getDate(String date){
		String y = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
		String m = Integer.toString(Calendar.getInstance().get(Calendar.MONTH)+1);
		String d = Integer.toString(Calendar.getInstance().get(Calendar.DATE));
		for(int i=y.length();i<4;i++){
			y = "0" + y;
		}
		for(int i=m.length();i<2;i++){
			m = "0" + m;
		}
		for(int i=d.length();i<2;i++){
			d = "0" + d;
		}
		switch(date.length()){
		case 0: date = y + m + d; break;
		case 3: date = y + "0" + date; break;
		case 4: date = y + date; break;
		case 6: date = y.substring(0,2) + date; break;
		case 8: break;
		default: return ""; // ERROR CASE MANAGING
		}
		return date;
	}
}