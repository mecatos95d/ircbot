package util.rail;

class StnTime{
	// Structure for each cell.
	int order; // Order from 0 to ...
	String name;
	String arrival;
	String depart;
	String delay;
	public StnTime(String[] input, int o) throws Exception{
		if(input.length != 4){ throw new Exception(); }
		order = o;
		name = input[0];
		arrival = input[1];
		depart = input[2];
		delay = input[3];
	}
	public int getWait(){
		if(arrival.length() == 1 || depart.length() == 1){ // Depart Stn or Arrival Stn
			return -1;
		}
		int h1 = Integer.parseInt(arrival.substring(0, 2));
		int m1 = Integer.parseInt(arrival.substring(3, 5));
		int h2 = Integer.parseInt(depart.substring(0, 2));
		int m2 = Integer.parseInt(depart.substring(3, 5));
		if(h1 > h2){ h2 += 24; }
		return 60 * (h2-h1) + (m2-m1);
	}
}