package util.rail;

import util.*;
import java.util.ArrayList;

class StnList{
	private ArrayList<StnTime> stns = new ArrayList<StnTime>();
	void add(StnTime st){
		stns.add(st);
	}
	int stnCode(String name){
		if(name.length() == 0){ return -2; }
		for(StnTime s : stns){
			if(Util.isSame(s.name, name)){ return s.order; }
		}
		return -1;
	}
	int getLength(){
		return stns.size();
	}
	String print(int code, int order1, int order2){
		// Initial Trick Part : at first station, arrival = depart
		int endCode = getLength()-1;
		stns.get(0).arrival = stns.get(0).depart;
		stns.get(endCode).depart = stns.get(endCode).arrival;
		
		final int changeLine = 100; // Line Change Length
		int leftLength = changeLine;
		int prevLength = 0;
		final int LONG_WAIT = 3;
		String str = "";
		int i=1;
		
		if(order1 == order2){ order2 = -2; } // Two stations are duplicated
		if(order1 == -2){ // No Station input
			switch(code){
			case 0: // Only Stn Name
				str = stns.get(0).name;
				for(;i<stns.size();i++){
					str += " - " + stns.get(i).name;
					leftLength -= (str.length() - prevLength); // right part is deltaLength
					if(leftLength <= 0){ str += "\n"; leftLength = changeLine; }
					prevLength = str.length();
				}
				break;
			case 1: // Stn Name and Time One
				str = stns.get(0).name + "(" + stns.get(0).depart + ")";
				for(;i<stns.size()-1;i++){
					str += " - " + stns.get(i).name + "(";
					if(stns.get(i).getWait() > LONG_WAIT){
						str += stns.get(i).arrival + "~";
					}
					str += stns.get(i).depart + ")";
					leftLength -= (str.length() - prevLength); // right part is deltaLength
					if(leftLength <= 0){ str += "\n"; leftLength = changeLine; }
					prevLength = str.length();
				}
				str += " - " + stns.get(i).name + "(" + stns.get(i).arrival + ")";
				break;
			case 2: // Stn Name and Time and Delay
				str = stns.get(0).name + "(" + stns.get(0).depart;
				if(!stns.get(0).delay.contains("-")){ str += "+" + stns.get(0).delay; }
				str += ")";
				for(;i<stns.size()-1;i++){
					str += " - " + stns.get(i).name + "(";
					if(stns.get(i).getWait() > LONG_WAIT){
						str += stns.get(i).arrival + "~";
					}
					str += stns.get(i).depart;
					if(!stns.get(i).delay.contains("-")){ str += "+" + stns.get(i).delay; }
					str += ")";
					leftLength -= (str.length() - prevLength); // right part is deltaLength
					if(leftLength <= 0){ str += "\n"; leftLength = changeLine; }
					prevLength = str.length();
				}
				str += " - " + stns.get(i).name + "(" + stns.get(i).arrival;
				if(!stns.get(i).delay.contains("-")){ str += "+" + stns.get(i).delay; }
				str += ")";
				break;
			}
		}
		else if(order2 == -2){ // One station input. Maximum 3 input, so no line change.
			switch(code){
			case 0: // Only Stn Name
				if(order1 != 0){ str += stns.get(0).name + " ---> "; }
				str += Constant.BOLD + stns.get(order1).name + Constant.BOLD;
				if(order1 != endCode){ str += " ---> " + stns.get(endCode).name; }
				break;
			case 1: // Stn Name and Time One
				if(order1 != 0){ str += stns.get(0).name + "(" + stns.get(0).depart + ") ---> "; }
				str += Constant.BOLD + stns.get(order1).name + "(";
				if(order1 != 0 && order1 != endCode){ str += stns.get(order1).arrival + "~"; }
				str += stns.get(order1).depart + ")" + Constant.BOLD;
				if(order1 != endCode){ str += " ---> " + stns.get(endCode).name + "(" + stns.get(endCode).arrival + ")"; }
				break;
			case 2: // Stn Name and Time and Delay
				if(order1 != 0){
					str += stns.get(0).name + "(" + stns.get(0).depart;
					if(!stns.get(0).delay.contains("-")){ str += "+" + stns.get(0).delay; }
					str += ") ---> ";
				}

				str += Constant.BOLD + stns.get(order1).name + "(";
				if(order1 != 0 && order1 != endCode){ str += stns.get(order1).arrival + "~"; }
				str += stns.get(order1).depart + ")" + Constant.BOLD;
				if(!stns.get(order1).delay.contains("-")){ str += "+" + stns.get(order1).delay; }
				
				if(order1 != endCode){
					str += " ---> " + stns.get(endCode).name + "(" + stns.get(endCode).arrival + ")";
					if(!stns.get(endCode).delay.contains("-")){ str += "+" + stns.get(endCode).delay; }
				}
				break;
			}
		}
		else{ // Two station input
			if(order1 > order2){ int tmp = order1; order1 = order2; order2 = tmp; } // Arrange

			switch(code){
			case 0: // Only Stn Name
				if(order1 != 0){ str += stns.get(0).name + " ---> "; }
				
				str += Constant.BOLD;
				for(i=order1;i<order2;i++){
					str += stns.get(i).name + " - ";

					leftLength -= (str.length() - prevLength); // right part is deltaLength
					if(leftLength <= 0){ str += "\n" + Constant.BOLD; leftLength = changeLine; }
					prevLength = str.length();
				}
				str += stns.get(order2).name + Constant.BOLD;

				leftLength -= (str.length() - prevLength); // right part is deltaLength
				if(leftLength <= 0){ str += "\n"; leftLength = changeLine; }
				prevLength = str.length();
				
				if(order2 != endCode){ str += " ---> " + stns.get(endCode).name; }
				break;
			case 1: // Stn Name and Time One
				if(order1 != 0){ str += stns.get(0).name + "(" + stns.get(0).depart + ") ---> "; }

				str += Constant.BOLD;
				for(i=order1;i<order2;i++){
					str += stns.get(i).name + "(";
					if(stns.get(i).getWait() > LONG_WAIT){
						str += stns.get(i).arrival + "~";
					}
					str += stns.get(i).depart + ") - ";

					leftLength -= (str.length() - prevLength); // right part is deltaLength
					if(leftLength <= 0){ str += "\n" + Constant.BOLD; leftLength = changeLine; }
					prevLength = str.length();
				}
				str += stns.get(order2).name + "(";
				
				if(stns.get(order2).getWait() > LONG_WAIT){
					str += stns.get(order2).arrival + "~";
				}
				str += stns.get(order2).depart + ")" + Constant.BOLD;

				leftLength -= (str.length() - prevLength); // right part is deltaLength
				if(leftLength <= 0){ str += "\n"; leftLength = changeLine; }
				prevLength = str.length();
				
				if(order2 != endCode){ str += " ---> " + stns.get(endCode).name + "(" + stns.get(endCode).arrival + ")"; }
				break;
			case 2: // Stn Name and Time and Delay
				if(order1 != 0){
					str += stns.get(0).name + "(" + stns.get(0).depart;
					if(!stns.get(0).delay.contains("-")){ str += "+" + stns.get(0).delay; }
					str += ") ---> ";
				}

				str += Constant.BOLD;
				for(i=order1;i<order2;i++){
					str += stns.get(i).name + "(";
					if(stns.get(i).getWait() > LONG_WAIT){
						str += stns.get(i).arrival + "~";
					}
					str += stns.get(i).depart;
					if(!stns.get(i).delay.contains("-")){ str += "+" + stns.get(i).delay; }
					str += ") - ";

					leftLength -= (str.length() - prevLength); // right part is deltaLength
					if(leftLength <= 0){ str += "\n" + Constant.BOLD; leftLength = changeLine; }
					prevLength = str.length();
				}
				str += stns.get(order2).name + "(";
				
				if(stns.get(order2).getWait() > LONG_WAIT){
					str += stns.get(order2).arrival + "~";
				}
				str += stns.get(order2).depart;
				if(!stns.get(order2).delay.contains("-")){ str += "+" + stns.get(order2).delay; }
				str += ")" + Constant.BOLD;

				leftLength -= (str.length() - prevLength); // right part is deltaLength
				if(leftLength <= 0){ str += "\n"; leftLength = changeLine; }
				prevLength = str.length();
				
				if(order2 != endCode){
					str += " ---> " + stns.get(endCode).name + "(" + stns.get(endCode).arrival;
					if(!stns.get(order2).delay.contains("-")){ str += "+" + stns.get(order2).delay; }
					str += ")";
				}
				break;
			}
		}
		return str;
	}
}