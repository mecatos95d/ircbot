package util;

import java.util.ArrayList;
import games.*;
import money.*;
import quiz.*;
import quiz.choquiz.ChoquizPlay;
import resource.*;

public class Constant{ // CONSTANTS and STATIC DATA
	// TEMPORARILY : USE ONLY IRCCLOUD
	public final static String ADMIN = "uid47596"; //"TOZ"; 
	public final static String ADMIN_NICK = "TÖZ57";
	public final static String NORMAL_NICK = "코코나";
	public final static String DEV_NICK = "나츠히";
	public final static String ID = "Ego_bot";
	public final static boolean ADMINmode = true; // mode for "leave" command
	public final static char BOLD = '';
	public final static char COLOR = '';
	public final static char ANTI_CALL = '∙';
	public final static int MAX_PERMU = 20;
	public final static String RG_DEFAULT = "TOZ57";
	public final static char[] CHOSUNG = {'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ',
			'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ','ㅌ', 'ㅍ', 'ㅎ'};

	public static ArrayList<String> BAN = new ArrayList<String>();

	public static ResourceStatus resource = new ResourceStatus();
	public static BlackjackGames blackjack = new BlackjackGames();
	public static MoneyDB money;
	
	public static SNQplay snq = new SNQplay();

	public static ArrayList<Chaser> ChaserPlay = new ArrayList<Chaser>();
	
}