package util;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

import struct.OrderCode;

public class WOTrating{
	private final static String HELP = "현재 입력된 유저의 World Of Tanks 레이팅을 출력해 주는 명령입니다. " + Constant.BOLD + "*레이팅 <유저명>" + Constant.BOLD
			+ "을 입력 시 해당 유저의 레이팅을 확인할 수 있습니다. (일부 유저의 경우는 별명을 사용해도 인식합니다).\n"
			+ "'레이팅' = SEA, '레이팅-' = SEA(부계정), '레이팅+' = KR(서버폐쇄, 명령 딜레이로 인하여 임시 봉인)";
	private final static String NotRegisteredError = "검색하려는 서버에 유저가 등록되어 있지 않습니다!";
	public static String[] order = {"레이팅", "rating", "월탱", "wot", "tank", "월", "레", "w", "t", "r"};
	private static String[][] option = { {""}, {"-"} }; //, {"+"} };
	
	private static String[] LINKS = {
			"http://wotinfo.net/en/efficiency?server=SEA&playername=", // Code 0 = SEA
			"http://wotinfo.net/en/efficiency?server=SEA&playername=", // Code 1 = SEA - Sub
			"http://wotinfo.net/en/efficiency?server=KR&playername=", // Code 2 = KR(off)
			//"http://wotbstars.com/?geo=Asia&lng=kor&usr="
	};
	private static String[] serverName = { "ASIA", "ASIA", "KR(off)" };
	private static String[][] nickChan = {
			{"TOZ57", "TÖZ57", "TOZ", "TÖZ", "토즈", "omxy-TOZ57"},
			{"ruby", "ruby3141", "CDnX", "엑스"},
			{"책읽는달팽", "Readingsnail", "RS", "책달", "책"},
			{"MeltDown", "MeltCraft", "Melda", "sarge", "멜다", "멜"},
			{"Saker", "DereIvn", "DereIvns", "DereDroid", "DereKU", "세이커", "데렙"}
	};
	private static String[][] Nick = {
			{ // Code 0 = SEA
			"TOZ57omxy",
			"CDnX",
			"",
			"Melda",
			"Derelvn"
			},
			{ // Code 1 = SEA - Sub
			"TOZBJORN",
			"CDnX",
			"",
			"Melda",
			"DereIvn"
			},
			{ // Code 2 = KR
			"TOZ57",
			"CDnX",
			"책읽는달팽",
			"MeltCraft",
			"DereIvn"
			}
			/*
			{ // Code 2 OLD = Blitz
			"TOZ57",
			"",
			"",
			"Melda",
			"DereIvn"
			}
			*/
	};
	private static int[][] prevBattle = new int[Nick.length][Nick[0].length];
	private static String[][] prevWR = new String[Nick.length][Nick[0].length];
	private static double[][][] prev = new double[Nick.length][Nick[0].length][3];
	public static int getCode(String input){
		String check = input.toLowerCase();
		for(int i=0;i<nickChan.length;i++){
			for(int j=0;j<nickChan[i].length;j++){
				if(Util.isSame(check, nickChan[i][j].toLowerCase())){
					return i;
				}
			}
		}
		return -1;
	}
	public static String message(String sender, ArrayList<String> content) throws Exception{
		int code = OrderCode.code(content.get(0), order, option);
		if(code == -1){ return ""; }
		
		String name_target = sender;
		if(content.size() > 1){
			name_target = content.get(1);
			if(Util.isSame(content.get(1), "?")){ return help(); }
		}
		String reply = rating(name_target, code);
		
		return reply;
	}
	public static void init(){
		for(int i=0;i<prev.length;i++){
			for(int j=0;j<prev[i].length;j++){
				prevBattle[i][j] = 0;
				prevWR[i][j] = "";
				for(int k=0;j<3;j++){
					prev[i][j][k] = 0; // This should be replaced by file I/O Reading
				}
			}
		}
	}
	public static String rating(String nick, int server) throws Exception {
		String coord = LINKS[server];
		int code = getCode(nick);
		if(code != -1){
			nick = Nick[server][code];
		}
		
		if(nick.length() == 0){ return NotRegisteredError(); }
		
		coord += URLEncoder.encode(nick, "UTF-8");
	
		// if(server == 2){ return coord; } // Blitz is not ready for parsing
		
		String answer = "Current rating of " + nick + "@" + serverName[server] + " | " + coord + "\n";
		
		URL page = new URL(coord);
		BufferedReader in = new BufferedReader(new InputStreamReader(page.openStream()));
		
		// SEE WOT_ReadResult_160217.txt.
		// Read result is VERY DIFFERENT FROM view-source from Chrome.
		// Below code is adjusted roughly.
		
		// Part 0 : Battles and Winrate
		while(true){
			String inputLine = in.readLine();
			if(inputLine.contains("tab-pane fade in active")){ break; }
		}
		for(int i=0;i<3;i++){
			in.readLine();
		}
		String battles = in.readLine().replaceAll("[^(0-9|\\(|\\))]", "").replaceAll("\\(.*\\)", "").trim();
		answer += "§" + battles + "(";
		
		for(int i=0;i<3;i++){
			in.readLine();
		}
		String wr = in.readLine().replaceAll("[^(0-9|\\.|%)]", "").trim();
		answer += wr + ") | ";
		
		// Part 1 : Eff
		while(true){
			String inputLine = in.readLine();
			if(inputLine.contains("glyphicon")){ break; }
		}
		for(int i=0;i<3;i++){
			in.readLine();
		}
		String eff = in.readLine().trim();
		answer += "Eff : " + eff; // Add Eff Value

		in.readLine();

		answer += " (" + solveRange(in.readLine()) + ") | ";

		// Part 2 : WN7
		while(true){
			String inputLine = in.readLine();
			if(inputLine.contains("WN7")){ break; }
		}

		for(int i=0;i<6;i++){
			in.readLine();
		}

		String wn7 = in.readLine().trim();
		answer += "WN7 : " + wn7; // Add WN7 Value

		in.readLine();

		answer += " (" + solveRange(in.readLine()) + ") | ";

		// Part 3 : WN8
		while(true){
			String inputLine = in.readLine();
			if(inputLine.contains("WN8")){ break; }
		}
		for(int i=0;i<5;i++){
			in.readLine();
		}

		String wn8 = in.readLine().trim();
		answer += "WN8 : " + wn8; // Add WN8 Value

		in.readLine();

		answer += " (" + solveRange(in.readLine()) + ")";

		in.close();
		
		if(code != -1){ // Temp setting. Change to compare condition
			int d_bat = parseBat(battles, code, server);
			String d_wr = parseWR(wr, code, server);
			double d_eff = parseNum(eff, code, server, 0);
			double d_wn7 = parseNum(wn7, code, server, 1);
			double d_wn8 = parseNum(wn8, code, server, 2);
//			System.out.println(d_eff + " " + d_wn7 + " " + d_wn8);
			if(d_bat != 0){
				answer += "\n";
				answer += "+" + d_bat + "(" + d_wr + ")";
				answer += " | Eff : " + printDouble(d_eff);
				answer += " | WN7 : " + printDouble(d_wn7);
				answer += " | WN8 : " + printDouble(d_wn8);
			}
		}
		return answer;
	}
	public static String winrate(String raw){
		return raw.substring(0, raw.length()-2) + "." + raw.substring(raw.length()-2) + "%";
	}
	public static int parseBat(String nVal, int code, int server){ // newVal, oldVal
		int dv = Integer.parseInt(nVal);
		int ret = dv - prevBattle[server][code];
		if(prevBattle[server][code] == 0){ ret = 0; }
		prevBattle[server][code] = dv;
		return ret;
	}
	public static String parseWR(String wr, int code, int server){
		String ret = "";
		if(!prevWR[server][code].isEmpty()){
			int del_wr = wr2int(wr) - wr2int(prevWR[server][code]);
			ret = int2wr(del_wr);
		}
		prevWR[server][code] = wr;
		return ret;
	}
	public static int wr2int(String wr){
		return Integer.parseInt(wr.replaceAll("[^(0-9)]", ""));
	}
	public static String int2wr(int wr){ // For delta wr
		if(wr == 0){ return "-"; }
		String wr_str = (Math.abs(wr) / 100) + "." + (Math.abs(wr)%100 < 10 ? "0" : "") + (Math.abs(wr) % 100) + "%";
		if(wr > 0){ return "+" + wr_str; }
		else { return "-" + wr_str; }
	}
	public static double parseNum(String nVal, int code, int server, int d){ // newVal, oldVal
		nVal = nVal.replace(",", "");
		double dv = Double.parseDouble(nVal);
		double ret = dv - prev[server][code][d];
		if(prev[server][code][d] == 0){ ret = 0; }
		prev[server][code][d] = dv;
		return ret;
	}
	public static String printDouble(double d){
		String s = "";
		if(d > 0){ s += "+"; }
		s += String.format("%.2f", d);
		return s;
	}
	public static String help(){
		return HELP;
	}
	public static String NotRegisteredError(){
		return NotRegisteredError;
	}
	public static String solveRange(String input){
		// Solve range name from below type source
		// Example :         <span class="label label-belowaverage">below average</span>
		int l = input.indexOf('>');
		int r = input.lastIndexOf('<');
		if(l == -1 || r == -1 || r < l){ return "?"; } // Error manage
		return input.substring(l+1, r);
	}
}