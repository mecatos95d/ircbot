package util;

import java.util.ArrayList;
import java.util.Random;

public class Util {
	/** Deleted
	public static String[] MCAnnounce = {
		"**", "*p", "*player", "*players", "*ㅔ",
		"@p", "@player", "@players", "@ㅔ",
		"!p", "!player", "!players", "!ㅔ",
		"!. p", "!. ㅔ"
	};
	*/
	public static String operBan(){
		return "사용 중지된 명령입니다.";
	}
	public static boolean isSame(String s1, String s2){ return s1.contains(s2) && s2.contains(s1); }
	public static String noCall(String sender){
		return sender.substring(0,1) + '∙' + sender.substring(1);
	}
	public static int dice(){
		Random r = new Random();
		return r.nextInt(6)+1;
	}
	public static boolean isAdmin(String login){
		return Util.isSame(login, Constant.ADMIN);
	}
	public static boolean isBanned(String nick, String login){
		for(int i=0;i<Constant.BAN.size();i++){
			if(Util.isSame(Constant.BAN.get(i), nick)){ return true; }
			if(Util.isSame(Constant.BAN.get(i), login)){ return true; }
		}
		return false;
	}
	public static boolean isBelow(char input){ // 받침이 있으면 true, 없으면 false
		return ((input-16) % 28 != 0); // 16 == 44032(mod 28)
	}
	public static String chosung(String input){
		int length = input.length();
		String initial = "";
		for(int i=0;i<length;i++){
			if(input.charAt(i) >= 44032 && input.charAt(i) <= 55203){
				initial += Constant.CHOSUNG[(input.charAt(i) - 44032) / 588];
			}
			else{ initial += input.charAt(i); }
		}
		return initial;
	}
	public static ArrayList<String> parse(String input){
		input = input.trim();
		input += " ";
		ArrayList<String> data = new ArrayList<String>();
		int crd = input.indexOf(" ");
		while(crd != -1){
			if(crd == 0){
				input = input.substring(1); // avoid consecutive space trap
			}
			else{
				data.add(input.substring(0, crd));
				input = input.substring(crd+1);
			}
			crd = input.indexOf(" ");
		}
		return data;
	}
	// opposite of parse
	public static String combine(ArrayList<String> input){
		if(input.size() == 0){ return ""; }
		String str = input.get(0);
		for(int i=1;i<input.size();i++){
			str += " " + input.get(i);
		}
		return str;
	}
	public static boolean isInt(double input){
		return (input - (int)input == 0);
	}
	public static boolean isStringDouble(String s) {
		try {
			Double.parseDouble(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	/**
	 * Return whether string(word) s is composed by 0~9.
	 * @param s(String)
	 * @return boolean (Whether string is composed by 0~9)
	 */
	public static boolean isNum(String s){
		for(int i=0;i<s.length();i++){
			int c = (int)s.charAt(i);
			if(c < 48 || c > 57){ return false; }
		}
		return true;
	}
	// return whether input string s is integer form.
	public static boolean isIntStr(String s){
		try{
			Integer.parseInt(s);
			return true;
		}
		catch(NumberFormatException e){
			return false; // s is not int form.
		}
	}
}
