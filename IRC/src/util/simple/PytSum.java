package util.simple;

import java.util.ArrayList;

import struct.OrderCode;
import util.Constant;
import util.Util;

public class PytSum{
	private final static String HELP = "입력된 숫자들의 제곱의 합의 제곱근을 출력합니다. 사용법 : *피타(pyt) [숫자1] [숫자2] ...";
	public final static String[] order = {"피타", "pyt"};
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order);
		if(c == -1){ return ""; }
		
		if(content.size() < 2){ return HELP; }
		if(Util.isSame(content.get(1), "?")){ return HELP; }
		
		int squareSum = 0;
		long squareSumCheck = 0;
		for(int i=1;i<content.size();i++){
			int x = -1;
			try{
				x = Integer.parseInt(content.get(i));
			} catch(NumberFormatException e){
				return "입력이 잘못되었습니다! : " + content.get(i);
			}
			if(x <= 0 || x > 46340){
				return "WARNING : 입력은 46340 이하의 양의 정수만 가능합니다.";
				// 46341^2 > INT_MAX
			}
			squareSum += x*x;
			squareSumCheck += (long)x*x;
			if(squareSumCheck - (long)squareSum != 0){
				return "WARNING : 범위를 초과했습니다!";
			}
		}
		int outNum = 1; int inNum = squareSum; // sqrt(squareSum) = outNum * sqrt(inNum) 
		while(inNum % 4 == 0){ inNum /= 4; outNum *= 2; }
		int q = 3; 
		while(q * q <= inNum){
			if(inNum % (q*q) == 0){ inNum /= (q*q); outNum *= q; }
			else{ q += 2; }
		}
		if(inNum == 1){ // Exact Integer
			return Constant.BOLD + String.valueOf(outNum) + Constant.BOLD;
		}
		else if(outNum == 1){
			return "√" + String.valueOf(inNum) + " = " + String.valueOf(Math.sqrt(squareSum)); }
		else{
			return "√" + String.valueOf(squareSum) + " = "
					+ String.valueOf(outNum) + "√" + String.valueOf(inNum) + " = "
					+ String.valueOf(Math.sqrt(squareSum));
		}
	}
}