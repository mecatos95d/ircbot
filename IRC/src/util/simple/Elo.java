package util.simple;

import java.util.ArrayList;

import struct.OrderCode;
import util.Util;

public class Elo{
	private final static String HELP = "ELO 레이팅 계산 명령입니다. 사용법 : *래더(elo) [승자 ELO] [패자 ELO]";
	public final static String[] order = {"래더", "elo"};
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order);
		if(c == -1){ return ""; }
		
		if(content.size() < 3){ return HELP; }
		if(Util.isSame(content.get(1), "?")){ return HELP; }
		
		double Ra = 0;
		double Rb = 0;
		try{
			Ra = (double)Integer.parseInt(content.get(1));
			Rb = (double)Integer.parseInt(content.get(2));
		} catch(NumberFormatException e){
			return "입력이 잘못되었습니다!";
		}
		
		double Qa,Qb,Ea,Eb,cRa,cRb,k=32;
		Qa=Math.pow(10,Ra/400); Qb=Math.pow(10,Rb/400);
		Ea=Qa/(Qa+Qb);Eb=Qb/(Qa+Qb);
		cRa=Ra+(k*(1-Ea)); cRb=Rb+(k*(0-Eb));
		return
				("Winner : " + Math.round(Ra) + " → " + Math.round(cRa))
				+ "\n" +
				("Loser : " + Math.round(Rb) + " → " + Math.round(cRb));
	}
}