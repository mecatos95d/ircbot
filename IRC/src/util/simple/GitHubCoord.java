package util.simple;

import java.util.ArrayList;

import struct.OrderCode;
import util.Util;

public class GitHubCoord{
	private final static String HELP = "GitHub 주소 반환 명령입니다. 실존하지 않더라도 주소를 반환합니다. 사용법 : *깃헙(github, git) <nick> [repository name]";
	public final static String[] order = {"깃헙", "github", "git", "깃", "g"};
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order);
		if(c == -1){ return ""; }
		
		if(content.size() < 2){ return HELP; }
		if(Util.isSame(content.get(1), "?")){ return HELP; }
		
		String coord = "https://github.com/";
		
		coord += content.get(1); // Nickname
		if(content.size() > 2){ coord += "/" + content.get(2); } // Reponame
		
		return coord;
	}
}