package util.simple;

import java.util.ArrayList;
import java.util.Calendar;

import struct.OrderCode;

public class TimePrint{
	public final static String[] order = {"시간", "time", "시각"};
	public final static String[] orderWS = {"몇시", "몇시?", "시각?", "시간?"};
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order);
		int c2 = OrderCode.codeWithoutStar(content.get(0), orderWS);
		if(c == -1 && c2 == -1){ return ""; }
		
		return Calendar.getInstance().getTime().toString();
	}
}