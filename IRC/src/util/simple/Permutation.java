package util.simple;

import java.util.ArrayList;
import java.util.Random;

import struct.OrderCode;
import util.Constant;
import util.Util;

public class Permutation{
	private final static String HELP = "무작위 순열을 출력해 주는 명령입니다. 사용법 : *순열(perm) [number] or *perm <arg1> <arg2> ...";
	public final static String[] order = {"순열", "rp", "perm"};
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order);
		if(c == -1){ return ""; }
		
		if(content.size() < 2){ return HELP; }
		if(Util.isSame(content.get(1), "?")){ return HELP; }
		
		boolean isList = true;
		int max = -1;
		if(content.size() == 2){
			try{
				isList = false;
				max = Integer.parseInt(content.get(1));
			} catch(NumberFormatException e){
				return "입력이 잘못되었습니다! : " + content.get(1);
			}
		}
		else{
			max = content.size()-1;
		}
		
		if(max <= 0){ return Constant.BOLD + "양의 정수를 입력하세요." + Constant.BOLD; }
		if(max > Constant.MAX_PERMU){
			return Constant.BOLD + "최대 입력 사이즈는 " + Constant.MAX_PERMU + "입니다." + Constant.BOLD;
		}
		Random r = new Random();
		int[] result = new int[max];
		String print = "Random ";
		if(isList){ print += "order"; }
		else{ print += "permutation"; }
		print += " :" + Constant.BOLD;
		for(int i=0;i<max;i++){
			boolean done = false;
			while(true){
				done = true;
				int rand = r.nextInt(max)+1;
				for(int j=0;j<i;j++){
					if(result[j]==rand){ done = false; break; }
				}
				if(done){
					result[i] = rand;
					print += " ";
					if(isList){ print += content.get(rand);}
					else{ print += rand; }
					break;
				}
			}
		}
		return print;
	}
}