package util.simple;

import java.util.ArrayList;

import struct.OrderCode;
import util.Util;

public class LogicSum{
	private final static String HELP = "로직 퍼즐에서 주어진 조건을 만족하도록 채우기 위해서 필요한 최소의 칸 수를 출력해 줍니다. "
			+ "사용법 : *로직(logic) [number1] [number2] ...";
	public final static String[] order = {"로직", "logic", "lg"};
	public static String message(String sender, ArrayList<String> content){
		int c = OrderCode.code(content.get(0), order);
		if(c == -1){ return ""; }
		
		if(content.size() < 2){ return HELP; }
		if(Util.isSame(content.get(1), "?")){ return HELP; }
		
		int sum = 0;
		for(int i=1;i<content.size();i++){
			int x = -1;
			try{
				x = Integer.parseInt(content.get(i));
			} catch(NumberFormatException e){
				return "입력이 잘못되었습니다! : " + content.get(i);
			}
			
			if(x <= 0){
				return "WARNING : 입력은 Integer 범위의 양의 정수만 가능합니다.";
			}
			sum += x+1;
		}
		return "최소 필요 칸 수 : " + sum + "칸";
	}
}