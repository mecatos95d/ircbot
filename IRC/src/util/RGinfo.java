package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

// RhythmGame Info Site Linking
public class RGinfo{ 
	private final String initfile = "./data/RGinfo/site.txt";
	private ArrayList<RG> list = new ArrayList<RG>();
	public void init() throws FileNotFoundException{
		Scanner inputStream = new Scanner(new File(initfile));
		while(inputStream.hasNextLine()){ list.add(new RG(inputStream.nextLine())); }
		inputStream.close();
	}
	public String process(ArrayList<String> content){
		for(int i=0;i<list.size();i++){
			for(int j=0;j<list.get(i).OPERCODE.size();j++){
				if(Util.isSame(content.get(0).toLowerCase(), "*" + list.get(i).OPERCODE.get(j))){
					if(content.size() == 0){ // Error manage
						return "";
					}
					if(content.size() == 1){
						return list.get(i).SITE + Constant.RG_DEFAULT;
					}
					else{
						return list.get(i).SITE + content.get(1);
					}
				}
			}
		}
		return "";
	}
}
class RG{
	public ArrayList<String> OPERCODE;
	public String SITE;
	RG(String s){
		ArrayList<String> parse = Util.parse(s);
		SITE = parse.get(0);
		parse.remove(0);
		OPERCODE = parse;
	}
}