package util;

public class TimeOper {
	final static String ErrorMessage = "";
	public static String operate(String input){
		int coord_m = input.indexOf("-");
		int coord_p = input.indexOf("+");
		int coord = -1;
		boolean subtract = false;
		if(coord_m != -1){ coord = coord_m; subtract = true; }
		else if(coord_p != -1){ coord = coord_p; }
		else{ return ""; }
		String time1 = input.substring(0,coord).trim();
		String time2 = input.substring(coord+1).trim();
		if(time2.contains("-")){ return ""; }
		Time t1 = parseTime(time1);
		Time t2 = parseTime(time2);
		Time result = null;
		if(subtract){ result = t1.subtract(t2); }
		else{ result = t1.add(t2); }
		if(result == null){ return ErrorMessage; }
		else{ return result.result(); }
	}
	public static Time parseTime(String input){
		int ampm = 0; // am : 1, pm : 2 24h : 0
		if(input.startsWith("AM")){ ampm = 1; input = input.substring(2).trim(); }
		if(input.startsWith("PM")){ ampm = 2; input = input.substring(2).trim(); }
		String[] part = input.split(":");
		if(part.length == 2){
			int hour = to24h(Integer.parseInt(part[0]), ampm);
			int min = Integer.parseInt(part[1]);
			if(isHourValid(hour) && isMinSecValid(min)){
				return new Time(hour, min);
			}
		}
		else if(part.length == 3){
			int hour = to24h(Integer.parseInt(part[0]), ampm);
			int min = Integer.parseInt(part[1]);
			int sec = Integer.parseInt(part[2]);
			if(isHourValid(hour) && isMinSecValid(min) && isMinSecValid(sec)){
				return new Time(hour, min, sec);
			}
		}
		return null;
	}
	static int to24h(int hour, int ampm){
		if(ampm == 0){ return hour; }
		else{
			if(hour <= 0 || hour >= 13){ return 99; } // AM0, PM0 is invalid
			return hour%12 + 12*(ampm-1);
		}
	}
	static boolean isHourValid(int hour){
		return hour < 24 && hour >= 0;
	}
	static boolean isMinSecValid(int input){
		return input < 60 && input >= 0;
	}
}
class Time{
	int hour;
	int min;
	boolean useSec;
	int sec;
	Time(int hour, int min){
		this.hour = hour; this.min = min;
		useSec = false; sec = 0;
	}
	Time(int hour, int min, int sec){
		this.hour = hour; this.min = min; this.sec = sec;
		useSec = true;
	}
	Time add(Time target){
		if(this.useSec != target.useSec){ return null; }
		Time tmp = null;
		if(this.useSec){
			tmp = new Time(this.hour + target.hour, this.min + target.min, this.sec + target.sec);
		}
		if(!this.useSec){
			tmp = new Time(this.hour + target.hour, this.min + target.min);
		}
		tmp.moderate();
		return tmp;
	}
	Time subtract(Time target){
		if(this.useSec != target.useSec){ return null; }
		Time tmp = null;
		if(this.useSec){
			tmp = new Time(this.hour - target.hour, this.min - target.min, this.sec - target.sec);
		}
		if(!this.useSec){
			tmp = new Time(this.hour - target.hour, this.min - target.min);
		}
		tmp.moderate();
		return tmp;
	}
	void moderate(){
		if(sec < 0){ min--; sec+=60; }
		if(sec >= 60){ min++; sec-=60; }
		if(min < 0){ hour--; min+=60; }
		if(min >= 60){ hour++; min-=60; }
		if(hour < 0){ hour += 24; }
		if(hour >= 24){ hour -= 24; }
	}
	String result(){
		String ret = hour + ":" + toString(min);
		if(useSec){
			ret += ":" + toString(sec);
		}
		return ret;
	}
	String toString(int val){
		if(val<10){ return "0" + val; }
		else{ return "" + val; }
	}
}