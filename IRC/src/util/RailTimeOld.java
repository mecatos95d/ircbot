package util;

import java.util.ArrayList;
import java.util.Calendar;

public class RailTimeOld{
	private final String page = "http://www.letskorail.com/ebizprd/EbizPrdTicketPr11131_i1.do?";
	private final String date = "txtRunDt=";
	private final String num = "&txtTrnNo=";
	private final String[] call = {"철도", "철도시간", "철도시각", "열차", "열차시간", "열차시각", "rail", "railtime"};
	
	public String process(ArrayList<String> message){
		if(message.size() < 1){ return null; }
		boolean run = false;
		for(int i=0;i<call.length;i++){
			if(Util.isSame("*" + call[i], message.get(0))){
				run = true; break;
			}
		}
		if(run){
			if(message.size() == 1){
				return "열차 시간표를 알려 주는 명령어입니다. 사용법 : "
						+ Constant.BOLD + "*열차 <번호> (<날짜>)\n예시) "
						+ Constant.BOLD + "*열차 001 20151231";
			}
			else if(message.size() == 2){
				String num = message.get(1);
				if(!Util.isNum(num)){
					return "입력이 잘못되었습니다. " + Constant.BOLD + "<번호>, <날짜>는 숫자로 이루어져 있어야 합니다.";
				}
				else if(num.length() > 5){
					return "입력이 잘못되었습니다. " + Constant.BOLD + "<번호>는 최대 5자리 숫자입니다.";
				}
				else{
					for(int i=num.length();i<5;i++){
						num = "0" + num;
					}

					String y = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
					String m = Integer.toString(Calendar.getInstance().get(Calendar.MONTH)+1);
					String d = Integer.toString(Calendar.getInstance().get(Calendar.DATE));
					for(int i=y.length();i<4;i++){
						y = "0" + y;
					}
					for(int i=m.length();i<2;i++){
						m = "0" + m;
					}
					for(int i=d.length();i<2;i++){
						d = "0" + d;
					}
					return page + date + (y+m+d) + this.num + num;
				}
			}
			else{
				String num = message.get(1);
				for(int i=num.length();i<5;i++){
					num = "0" + num;
				}
				
				String date = message.get(2);
				// Trying to parse 'm/d' case
//				if(date.contains("/")){
//					int c = date.indexOf('/');
//					String month = date.substring(0, c);
//					String day = date.substring(c+1);
//					if(Util.isNum(month) && Util.isNum(day)){
//						
//					}
//					else{
//						
//					}
//				}
				String y = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
				switch(date.length()){
				case 3: date = y + "0" + date; break;
				case 4: date = y + date; break;
				case 6: date = y.substring(0,2) + date; break;
				case 8: break;
				default: return "날짜 입력이 잘못된 것 같습니다!";
				}
				return page + this.date + date + this.num + num;
			}
		}
		else{
			return null;
		}
	}	
}
