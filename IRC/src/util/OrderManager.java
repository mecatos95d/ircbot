package util;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import util.simple.*;
import util.rail.RailTime;
import quiz.choquiz.Choquiz;

public class OrderManager{
	private static ArrayList<String> orderList = new ArrayList<String>(); // short version. *Korean(English)
	private static ArrayList<String> fullOrder = new ArrayList<String>(); // Full version. For Debugging
	public static String message(String channel, String sender, ArrayList<String> content){
		String reply = "";
		
		try {
			reply += WOTrating.message(sender, content);
		}
		catch(Exception e){}
		if(reply.length() != 0){ return reply; }
		
		try {
			reply += RailTime.message(sender, content);
		}
		catch(Exception e){}
		if(reply.length() != 0){ return reply; }
		
		reply += Twitch.message(sender, content);
		reply += Youtube.message(sender, content);
		if(reply.length() != 0){ return reply; } // Time manage

		reply += Elo.message(sender, content);
		reply += GitHubCoord.message(sender, content);
		reply += Permutation.message(sender, content);
		reply += PytSum.message(sender, content);
		reply += LogicSum.message(sender, content);
		reply += TimePrint.message(sender, content);
		if(reply.length() != 0){ return reply; }
		
		reply += Choquiz.message(channel, sender, content);
		reply += Choquiz.choquizAnswerCheck(channel, sender, content);
		if(reply.length() != 0){ return reply; }
		
		
		
		return reply;
	}
	public static void init(){ // Initialize Classes
		try{ Choquiz.init(); }
		catch(FileNotFoundException e){
			e.printStackTrace();
			System.out.println("초성퀴즈 초기화 실패! 파일을 읽어오지 못하였습니다!");
		}
		initOrder();
		WOTrating.init();
	}
	public static void initOrder(){
		addOrder(WOTrating.order);
		addOrder(Twitch.order);
		addOrder(Youtube.order);
		
		addOrder(Elo.order);
		addOrder(GitHubCoord.order);
		addOrder(Permutation.order);
		addOrder(PytSum.order);
		addOrder(LogicSum.order);
		addOrder(Choquiz.order);
		addOrder(TimePrint.order);
	}
	public static void addOrder(String[] order){
		orderList.add("*" + order[0] + "(" + order[1] + ")");
		for(int i=0;i<order.length;i++){
			fullOrder.add(order[i]);
		}
	}
}