package quiz.choquiz;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import struct.OrderCode;
import util.Constant;
import util.Util;

public class Choquiz{
	private final static String HELP = "초성 퀴즈 기능입니다. 세부 명령들은 " + Constant.BOLD + "*초퀴즈 명령" + Constant.BOLD + "을 참조하세요.";
	public final static String[] order = {"초퀴즈", "choquiz"};
	private final static String[][] option = {
			{"시작", "start"},
			{"종료", "중단", "quit"},
			{"문제", "question", "problem"},
			{"통과", "패스", "skip", "pass"},
			{"힌트", "hint"},
			{"분야", "장르", "genre"},
			{"출처", "공헌", "source"},
			{"도움말", "도움", "help"},
			{"명령", "명령어", "order"},
	};
	
	// Common Error Messages
	private final static String noChoquiz = "초성퀴즈가 진행중이지 않습니다!";
	
	// Choquiz Database. Initialized at init()
	public static ArrayList<String>[] choquizDB = new ArrayList[ChoquizConst.choquizType.length]; // one way : convert each type to class(later)
	// Choquiz Storage
	public static ArrayList<ChoquizPlay> choquizRunning = new ArrayList<ChoquizPlay>();

	public static void init() throws FileNotFoundException{		
		String filename;
		for(int i=0;i<ChoquizConst.choquizType.length;i++){
			choquizDB[i] = new ArrayList<String>();
			filename = "./data/choquiz/" + ChoquizConst.choquizType[i] + ".txt";
			Scanner inputStream = new Scanner(new File(filename));
			while(inputStream.hasNextLine()){
				choquizDB[i].add(inputStream.nextLine());
			}
			inputStream.close();
		}
		System.out.println("초성퀴즈 초기화 완료");
	}
	/**
	 * return current channel's choquiz coord in "choquizRunning".
	 * If not running, return -1.
	 */
	public static int getChoquizCoord(String channel){
		for(int i=0;i<choquizRunning.size();i++){
			if(Util.isSame(choquizRunning.get(i).channel, channel)){ return i; }
		}
		return -1;
	}
	/**
	 * return current type's coord in "choquizRunning".
	 * If not in choquiz Type, return -1.
	 */
	public static int getChoquizTypeCode(String type){
		for(int i=0;i<ChoquizConst.choquizType.length;i++){
			if(Util.isSame(ChoquizConst.choquizType[i], type)){ return i; }
		}
		return -1;
	}
	/**
	 * Delete specific channel's running choquiz from "choquizRunning".
	 * If deleted successfully, then return 0.
	 * If error occurred(not started yet), then return -1.
	 */
	public static int removeChoquiz(String channel){
		int coord = getChoquizCoord(channel);
		if(coord == -1){ return -1; }
		else{ choquizRunning.remove(coord); return 0; }
	}
	/**
	 * Check Answer and pass to next Quiz
	 */
	public static String choquizAnswerCheck(String channel, String sender, ArrayList<String> content){
		if(getChoquizCoord(channel) == -1){ return ""; }
		ChoquizPlay current = choquizRunning.get(getChoquizCoord(channel));
		String msg = Util.combine(content);
		if(Util.isSame(msg, current.answer)){
			return "[초성퀴즈] 정답 : " + current.answer + " | 정답자 : " + Util.noCall(sender) + "("
					+ current.addScore(sender) + "점)\n" + current.nextQuestion(channel);
		}
		return ""; // Not answer
	}
	
	public static String message(String channel, String sender, ArrayList<String> content){
		String msg = choquizAnswerCheck(channel, sender, content);

		if(msg.length() != 0){ return msg; } // Answer is correct.
		
		if(content.size() < 2){ content.add(""); content.add(""); } // When Not Enough input parameters. content.get(1) do not exist.
		
		int c = OrderCode.code(content.get(0), content.get(1), order, option);
		if(c == -1){ return ""; } // 명령이 아닌 경우 무시
		if(c == -2){ return HELP; } // *초퀴즈 이후 들어오는 parameter가 적합하지 않은 경우

		if(Util.isSame(content.get(1), "?")){ return HELP; } // When "*order ?" case.
		
		int coord = getChoquizCoord(channel);
		switch(c){ // Refer order of option[]
		case 0: // "*초퀴즈 시작 <장르> <문제수>
			if(coord != -1){
				return "이미 초성퀴즈가 진행중입니다. 종료를 원하실 경우, " + Constant.BOLD + 
						"*초퀴즈 종료" + Constant.BOLD + "를 입력해주세요.";
			}
			else{
				if(content.size() < 3){ // "*초퀴즈 시작" 만 입력한 경우
					return "시작할 초성퀴즈 분야를 입력해주세요. "
						+ "초퀴즈 분야 목록은 (*초퀴즈 분야) 명령을 통해서 확인 할 수 있습니다. "
						+ "명령) *초퀴즈 시작 <장르> <문제수>";
				}
				String type = content.get(2);
				int typeCode = getChoquizTypeCode(type);
				if(typeCode == -1){
					return "\"" + type + "\" 분야는 초성퀴즈 DB에 존재하지 않습니다."
							+ "분야 목록은 " + Constant.BOLD + "*초퀴즈 분야" + Constant.BOLD + " 로 확인 가능합니다.";
				}
				int questions = ChoquizConst.MAX_QUIZ;
				if(content.size() < 4){ // No question number input
					// Do nothing : questions is already set.
				}
				else{
					try{ questions = Integer.parseInt(content.get(3)); }
					catch(NumberFormatException e){ return "문제 수 입력이 잘못되었습니다. 올바른 명령) *초퀴즈 시작 <장르> <문제수>"; }
				}
				if(questions > 0){
					if(questions > ChoquizConst.MAX_QUIZ){
						questions = ChoquizConst.MAX_QUIZ;
						msg += "한 번에 시작 할 수 있는 초퀴즈 문제 수는 "
								+ Constant.BOLD + ChoquizConst.MAX_QUIZ + Constant.BOLD + " 문제를 넘을 수 없습니다. ";
					}
					msg += type +" 분야의 초성퀴즈 " + questions + "문제가 시작되었습니다.\n";
					ChoquizPlay cpt = new ChoquizPlay(channel, typeCode, questions);
					msg += cpt.nextQuestion(channel);
					choquizRunning.add(cpt);
					return msg;
				}
				else{ return "문제 수 입력이 잘못되었습니다."; }
			}
		case 1: // *초퀴즈 종료
			// 이 부분은 순서를 바꿔서 깔끔하게 할 수 있지만 유저 편의성을 위해서 문제 답/스코어 출력
			if(coord == -1){ // no choquiz
				return noChoquiz;
			}
			else{
				ChoquizPlay current = choquizRunning.get(coord);
				msg += "진행중이던 초성퀴즈가 종료되었습니다.\n";
				msg += "[초성퀴즈] 정답 : " + current.answer +"\n";
				msg += current.returnGrade();
				removeChoquiz(channel);
			}
			return msg;
		case 2: // *초퀴즈 문제
			if(coord == -1){
				return noChoquiz;
			}
			else{
				return choquizRunning.get(coord).printQuestion();
			}
		case 3: // *초퀴즈 통과
			if(coord == -1){ // no choquiz
				return noChoquiz;
			}
			else{
				ChoquizPlay current = choquizRunning.get(coord);
				return "[초성퀴즈] 이번 문제를 건너뜁니다. 정답 : " + current.answer + "\n" + current.nextQuestion(channel);
			}
		case 4: // *초퀴즈 힌트
			if(coord == -1){
				return noChoquiz;
			}
			else{
				return choquizRunning.get(coord).hint();
			}
		case 5: // *초퀴즈 분야
			String msg5 = "현재 사용 가능한 초성퀴즈 분야 목록 :";
			for(int i=0;i<ChoquizConst.choquizType.length;i++){
				msg5 += " " + ChoquizConst.choquizType[i];
			}
			return msg5;
		case 6: // *초퀴즈 공헌
			return ChoquizConst.choquizReference;
		case 7: // *초퀴즈 도움말
			return "초성 퀴즈(초성이 주어지면 원래 단어를 유추하는 퀴즈)를 풀 수 있습니다. "
					+ "\"*초퀴즈 시작 <분야> <문제수>\"로 초성퀴즈를 시작 할 수 있으며, 정답이 대화방에 나오게 되면 바로 다음 문제로 넘어가게 됩니다.\n"
					+ "그 외의 세부 명령은 \"*초퀴즈 명령\"을 통해서 확인하실 수 있습니다.";
		case 8: // *초퀴즈 명령
			String msg8 = "";
			msg8 += Constant.BOLD + "*초퀴즈 시작 <분야> <문제수>" + Constant.BOLD + " | 해당 <분야>의 <문제수>만큼의 초성퀴즈를 시작합니다. //// ";
			msg8 += Constant.BOLD + "*초퀴즈 종료" + Constant.BOLD + " | 현재 진행중인 초성퀴즈를 종료합니다.\n";
			msg8 += Constant.BOLD + "*초퀴즈 문제" + Constant.BOLD + " | 현재 진행중인 초성퀴즈 문제를 확인합니다.\n";
			msg8 += Constant.BOLD + "*초퀴즈 통과" + Constant.BOLD + " | 현재 진행중인 초성퀴즈 문제를 패스하고 답을 확인합니다. //// ";
			msg8 += Constant.BOLD + "*초퀴즈 힌트" + Constant.BOLD + " | 현재 진행중인 초성퀴즈 문제의 힌트를 봅니다.\n";
			msg8 += Constant.BOLD + "*초퀴즈 분야" + Constant.BOLD + " | 현재 등록된 모든 초성퀴즈 분야를 확인합니다. //// ";
			msg8 += Constant.BOLD + "*초퀴즈 공헌" + Constant.BOLD + " | 각 초성퀴즈 분야의 출처/공헌자를 확인합니다.\n";
			return msg8;
		default:
			return "";
		}
	}
}