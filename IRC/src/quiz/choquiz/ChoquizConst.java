/**
 * After adding/removing choquiz,
 *	Only need to change this file.
 */

package quiz.choquiz;

import java.util.ArrayList;

public class ChoquizConst{
	public final static int MAX_QUIZ = 50;
	
	public final static String[] choquizType = // Should be one word.
		{"행정구역", "원소", "언어", "고속도로", "도쿄철도역", "일본철도역", "일본철도", "프로야구단", "한국스포츠팀", "열매", "조류"};
	public final static String choquizReference = " \"행정구역\" : Thanks to 책∙읽는달팽 and Wikipedia \n" +
			" \"프로야구단\" : KBO, NPB, MLB \n" +
			" \"한국스포츠팀\" : KBO(야구), K리그 클래식/챌린지(축구), KBL(농구), V-리그(배구)" +
			" / 준프로팀(공기업 소속팀)도 포함 \n" +
			" \"원소, 고속도로\" : From Wikipedia <주기율표>, <고속국도> \n" +
			" \"열매, 조류\" : Thanks to L∙emminkäinen \n" +
			" \"언어(프로그래밍 언어)\" : Thanks to l∙ihe and J∙iminP \n" +
			" \"도쿄철도역, 일본철도역, 일본철도\" : Thanks to 아∙즈사";
}