package quiz.choquiz;
import java.util.ArrayList;
import java.util.Random;

import util.*;

class ChoquizScore{
	String name;
	int score;
	ChoquizScore(String name){ this.name = name; score = 1; }
}

public class ChoquizPlay{
	public String channel;
	public int typeCode;
	public int questions; // how many questions left
	public String answer;
	private String initial;
	public int maxHint;
	public int openHint;
	public ArrayList<ChoquizScore> grade = new ArrayList<ChoquizScore>();
	public ChoquizPlay(String channel, int typeCode, int questions){
		this.channel = channel; this.typeCode = typeCode; this.questions = questions; this.answer = "";
		grade.clear();
	}
	//isSame : just for "answer()"
	public int addScore(String player){ // When player got answer,
		for(int i=0;i<grade.size();i++){
			if(Util.isSame(grade.get(i).name, player)){
				grade.get(i).score++; return grade.get(i).score;
			}
		}
		grade.add(new ChoquizScore(player));
		return 1;
	}
	public String nextQuestion(String channel){
		if(questions == 0){
			Choquiz.removeChoquiz(channel);
			return "초성퀴즈가 모두 종료되었습니다.\n" + returnGrade();
		}
		else{
			questions--; int cases = Choquiz.choquizDB[typeCode].size();
			while(true){
				int questionNo = new Random().nextInt(cases);
				String nextQ = Choquiz.choquizDB[typeCode].get(questionNo);
				if(!Util.isSame(nextQ, answer)){ // When question is duplicated
					answer = nextQ;
					break;
				}
			}
			initial = Util.chosung(answer);
			System.out.println(answer + " : " + initial); // For answer checking - Used at debugging.
			openHint = 0; maxHint = (length() - 1) / 2;
			return printQuestion();
		}
	}
	public String printQuestion(){
		return "[초성퀴즈] 문제 : " + initial;
	}
	public String hint(){
		if(openHint >= maxHint ){
			return "힌트를 모두 사용하였기 때문에 더 이상의 힌트는 없습니다. (최대 힌트 " + maxHint + "개)";
		}
		else{
			while(true){
				int pos = new Random().nextInt(initial.length());
				char openChar = initial.charAt(pos);
				if(openChar >= 12593 && openChar <= 12622){ // If picked one is not opened && chosung case
					initial = initial.substring(0, pos) + answer.charAt(pos) + initial.substring(pos+1);
					openHint++;
					return "힌트 : " + initial;
				}
			}
		}
		
	}
	private int length(){ // length of korean char part
		int hanguel = 0;
		for(int i=0;i<answer.length();i++){
			if(answer.charAt(i) >= 44032 && answer.charAt(i) <= 55203){ hanguel++; }
		}
		return hanguel;
	}
	public String returnGrade(){
		String scores = "득점 순위 : ";
		if(grade.size() == 0){ return ""; } // 정답자 없는경우
		//Sorting part
		ChoquizScore temp; // to change coordinates
		for(int i=1;i<grade.size();i++){
			for(int j=0;j<i;j++){
				if(grade.get(i).score > grade.get(j).score){
					temp = grade.get(i);
					grade.set(i, grade.get(j));
					grade.set(j, temp);
				}
			}
		}
		//Score answering part
		int max_score = 0; boolean isBold = true;
		if(grade.size() != 0){ max_score = grade.get(0).score; }
		for(int i=0;i<grade.size();i++){
			if(grade.get(i).score != max_score && isBold){ scores += ""; isBold = false; }
			if(i != 0){ 	scores += " | "; }
			scores += Util.noCall(grade.get(i).name) + "(" + grade.get(i).score + "점)";
		}
		return scores;
	}
}