package quiz;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import util.Util;

class Line{
	private String zone;
	private String lineName;
	private int color;
	Line(String zone, String lineName, int color){
		this.zone = zone; this.lineName = lineName; this.color = color;
	}
	String getZone(){ return zone; }
	String getName(){ return "" + color + " " + lineName + ""; }
	int getColor(){ return color; }
	String fileName(){
		return "./data/StnNoQuiz/" + zone + "/" + lineName + ".txt";
	}
}
public class SNQ{
	private ArrayList<Pair> DB = new ArrayList<Pair>();
	private void readLine(Line line) throws FileNotFoundException{
		Scanner inputStream = new Scanner(new File(line.fileName()));
		while(inputStream.hasNextLine()){
			ArrayList<String> pair = Util.parse(inputStream.nextLine());
			getDB().add(new Pair(pair.get(0), pair.get(1), line.getZone(), line.getName()));
		}
		inputStream.close();
	}
	private void getLines() throws FileNotFoundException{
		String filename = "./data/StnNoQuiz/Lines.txt";
		Scanner inputStream = new Scanner(new File(filename));
		while(inputStream.hasNextLine()){
			ArrayList<String> lines = Util.parse(inputStream.nextLine());
			for(int i=1;i<lines.size();i++){
				int coord = lines.get(i).indexOf("/");
				readLine(new Line(lines.get(0), lines.get(i).substring(0, coord),
						Integer.parseInt(lines.get(i).substring(coord+1))));
			}
		}
		System.out.println("SNQ 읽기 완료");
		inputStream.close();
	}
	private void init() throws FileNotFoundException{ getLines(); }
	public SNQ(){
		try{ init(); }
		catch(FileNotFoundException e){}
	}
	public ArrayList<Pair> getDB() {
		return DB;
	}
	public void setDB(ArrayList<Pair> dB) {
		DB = dB;
	}
}