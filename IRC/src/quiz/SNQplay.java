package quiz;

import java.util.ArrayList;
import java.util.Random;

import util.Util;

class Const{ // 주요 설정용 상수들
	public static int MAX_PERSON = 10;
	public static int MAX_QUIZ = 50;
}

class Pair{
	private String num;
	private String name;
	private String zone;
	private String line;
	public String getNum(){ return num; }
	public String getName(){ return name; }
	public String problem(){ return num + "(" + zone + " /" + line + ")"; }
	Pair(String num, String name, String zone, String line){
		this.num = num; this.name = name; this.zone = zone; this.line = line;
	}
}
class Score{
	private String nick;
	private int score;
	public Score(String nick){ this.nick = nick; score = 1; }
	public String getNick(){ return nick; }
	public int getScore(){ return score; }
	public void add(){ score++; }
}
class SNQset{
	private String channel;
	private int num; // 잔여 문제(현 문제 포함)
	private Pair set;
	private ArrayList<Score> score = new ArrayList<Score>();
	SNQset(String channel, int num, Pair set){ this.channel = channel; this.num = num; this.set = set; }
	public void setNum(int num){ this.num = num; }
	public int getNum(){ return num; }
	public String getChan(){ return channel; }
	public String getAns(){ return set.getName(); }
	public String getProb(){ return set.problem(); }
	public ArrayList<Score> getScore(){ return score; }
	public void sort(){
		for(int i=0;i<Math.min(Const.MAX_PERSON,score.size()-1);i++){
			for(int j=i+1;j<score.size();j++){
				Score A = score.get(i); Score B = score.get(j);
				if(A.getScore() < B.getScore()){
					Score tmp = A; A = B; B = tmp;
				}
			}
		}
	}
	public void newQ(Pair pair){ num--; set = pair; }
}
public class SNQplay{
	private ArrayList<SNQset> data = new ArrayList<SNQset>();
	static SNQ DB = new SNQ();
	private Pair randQ(){
		Random r = new Random();
		return DB.getDB().get(r.nextInt(DB.getDB().size()));
	}
	private String newQ(String channel){
		int coord = -1;
		for(int i=0;i<data.size();i++){
			if(Util.isSame(data.get(i).getChan(), channel)){
				coord = i;
			}
		}
		if(coord == -1){ return notPlaying(); } // non-progressing
		if(data.get(coord).getNum() == 1){ return endGame(coord); } // remove
		data.get(coord).newQ(randQ());
		return "문제 : " + problem(channel);
	}
	private String endGame(int coord){
		String scores = "퀴즈가 종료되었습니다.\n득점 순위 : ";
		ArrayList<Score> grade = data.get(coord).getScore();
		if(grade.isEmpty()){ data.remove(coord); return "퀴즈가 종료되었습니다."; } // 정답자 없는경우
		data.get(coord).sort();
		int max_score = 0; boolean isBold = true;
		if(grade.size() != 0){ max_score = grade.get(0).getScore(); }
		for(int i=0;i<Math.min(Const.MAX_PERSON, grade.size());i++){
			if(grade.get(i).getScore() != max_score && isBold){ scores += ""; isBold = false; }
			if(i != 0){ 	scores += " | "; }
			scores += Util.noCall(grade.get(i).getNick()) + "(" + grade.get(i).getScore() + "점)";
		}
		data.remove(coord);
		return scores;
	}
	private String addScore(String nick, String channel, String ans){
		ArrayList<Score> tmp = null;
		for(int i=0;i<data.size();i++){
			if(Util.isSame(data.get(i).getChan(), channel)){ tmp = data.get(i).getScore(); break; }
		}
		for(int i=0;i<tmp.size();i++){
			if(Util.isSame(tmp.get(i).getNick(), nick)){
				tmp.get(i).add();
				return "정답 : " + ans + ", 정답자 : " + Util.noCall(nick) + "(" + tmp.get(i).getScore() + "점)";
			}
		}
		// When first answer
		tmp.add(new Score(nick));
		return "정답 : " + ans + ", 정답자 : " + Util.noCall(nick) + "(1점)";
	}
	private String answer(String channel){
		for(int i=0;i<data.size();i++){
			if(Util.isSame(data.get(i).getChan(), channel)){
				return data.get(i).getAns();
			}
		}
		return null;
	}
	private String problem(String channel){
		for(int i=0;i<data.size();i++){
			if(Util.isSame(data.get(i).getChan(), channel)){
				return "" + data.get(i).getProb() + "";
			}
		}
		return null;
	}
	private String notPlaying(){ return "SNQ퀴즈가 진행중이지 않습니다!"; }
	public String process(String sender, String channel, String message){
		String ans = answer(channel);
		boolean play = (ans != null);
		message = message.trim().toLowerCase();
		if(play){
			if(Util.isSame(ans, message) ||
					(Util.isSame(ans+"역", message) && message.charAt(ans.length()-1)!='역')){
				return addScore(sender, channel, ans) + "\n" + newQ(channel);
			}
		}
		message = message.replace("*눕", "*snq");
		if(message.startsWith("*snq")){
			if(message.startsWith("*snq 시작") || message.startsWith("*snq start")
					|| message.startsWith("*snq begin")){
				if(play){ return "이미 SNQ퀴즈가 진행중입니다. 종료를 원하실 경우, *SNQ 종료를 입력해주세요."; }
				else{
					int quizNo = Integer.parseInt(Util.parse(message).get(2));
					String reply = "";
					if(quizNo <= 0){
						return "문제 수 입력이 잘못되었습니다 : " + quizNo; 
					}
					if(quizNo > Const.MAX_QUIZ){
						reply = "한 번엔 최대 " + Const.MAX_QUIZ + "문제를 풀 수 있습니다!\n" +
								"자동으로 " + Const.MAX_QUIZ + "문제로 변환됩니다.\n"; 
						quizNo = Const.MAX_QUIZ;
					}
					data.add(new SNQset(channel, quizNo, randQ()));
					reply += "문제 : " + problem(channel);
					return reply;
				}
			}
			else if(message.startsWith("*snq 정보") || message.startsWith("*snq?") || message.startsWith("*snq란?")
				|| message.startsWith("*snq 도움") || message.startsWith("*snq info") || message.startsWith("*snq help")){
				return "SNQ는 Station Number Quiz의 약어로 대한민국 도시철도상에서 주어진 역번호가 무슨 역인지" +
						"맞추는 퀴즈입니다.\n" +
						"각 문제의 힌트로는 초성이 주어집니다. 명령들은 *SNQ 명령을 참조하세요.\n"+
						"현재 " + DB.getDB().size() + "개의 역 데이터가 있습니다.";
			}
			else if(message.startsWith("*snq 종료") || message.startsWith("*snq 중단") 
					|| message.startsWith("*snq 그만") || message.startsWith("*snq 포기")
					|| message.startsWith("*snq stop") || message.startsWith("*snq abandon")
				|| message.startsWith("*snq quit") || message.startsWith("*snq end") || message.startsWith("*snq gg")){
				if(!play){ return notPlaying(); }
				else{
					int coord = -1;
					for(int i=0;i<data.size();i++){
						if(Util.isSame(data.get(i).getChan(), channel)){
							coord = i;
						}
					}
					return problem(channel) + " 역은 " + ans + " 입니다." + endGame(coord);
				}
			}
			else if(message.startsWith("*snq 통과") || message.startsWith("*snq 패스")
					|| message.startsWith("*snq pass")){
				if(!play){ return notPlaying(); }
				else{
					return "이 문제를 통과합니다. " + problem(channel) + " 역은 " + ans + " 입니다.\n"
							+ newQ(channel);
				}
			}
			else if(message.startsWith("*snq 문제") || message.startsWith("*snq problem")
					|| message.startsWith("*snq question")){
				if(!play){ return notPlaying(); }
				else{ return "현재 문제는 " + problem(channel) + " 입니다."; }
			}
			else if(message.startsWith("*snq 힌트") || message.startsWith("*snq hint")){
				if(!play){ return notPlaying(); }
				else{ return "현재 문제의 힌트는 " + Util.chosung(ans) + " 입니다."; }
			}
			else if(message.startsWith("*snq 명령") || message.startsWith("*snq order")){
				return "*snq 시작 <문제수> / *snq start <문제수> : SNQ퀴즈를 시작합니다.\n" +
						"*snq 정보 / *snq info : SNQ퀴즈 정보를 봅니다.\n" +
						"*snq 종료 / *snq quit : SNQ퀴즈를 종료합니다.\n" +
						"*snq 통과 / *snq pass : SNQ퀴즈 한 문제를 통과합니다.\n" +
						"*snq 문제 / *snq question : SNQ퀴즈 현재 문제를 확인합니다.\n" +
						"*snq 힌트 / *snq hint : SNQ퀴즈 현재 문제의 힌트(초성)을 확인합니다.\n" +
						"*snq 명령 / *snq order : SNQ퀴즈 명령어를 확인합니다.";
			}
			else{
				return "SNQ(역번호퀴즈) 기능입니다. 자세한 것은 *SNQ 정보를 참조하세요.";
			}
		}
		return "";
	}
}