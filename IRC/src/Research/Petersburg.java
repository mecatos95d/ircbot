package Research;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**St. Petersburg Paradox Game
 * 
 * http://ko.wikipedia.org/wiki/상트페테르부르크의_역설
 * 기대값이 무한인 게임에서 실험적인 기대값을 구하는게 목적.
 * 
 * 0단계 실패시 보상 0, 1단계 실패시 보상 1을 기본으로 한다.
 *------- 명령들
 * *상페 <난이도> - 0~(난이도-1) 중에서 고르는 문제. 보상은 각 단계마다 (난이도)배. 기대값 무한.
 * *상페2 - 0~3 중에서 고르는 문제. 보상은 각 단계마다 2배. 기대값이 1/2 = (1/4 + 1/8 + ...)
 */
class Petersburg{
	
}
class PetersburgGames{
	ArrayList<Petersburg> games = new ArrayList<Petersburg>(); // Game Execution을 위한 부분.
	String fileName = "./data/research/petersburg.txt";
	int trial; long sum; double expectation; // trial = 시도 회수, sum = 번 돈의 누적, expectation = 현재까지 누적 기대값
	PetersburgGames() throws FileNotFoundException{ // Load trial, sum, expectation.
		Scanner input = new Scanner(new File(fileName));
		trial = input.nextInt(); sum = input.nextLong(); expectation = input.nextDouble();
	}
}